<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$failedTransactionsTable = $installer->getConnection()
    ->newTable($installer->getTable('retaincustomerdetails/failedtransactions'))
    ->addColumn(
        'entity_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ),
        'Unique identifier'
    )
    ->addColumn(
        'created_at',
        Varien_Db_Ddl_Table::TYPE_DATE,
        null,
        array(
            'nullable' => false
        ),
        'Created At'
    )
    ->addColumn(
        'customer_id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        null,
        array(
            'nullable' => true
        ),
        'Customer Id'
    )
    ->addColumn(
        'customer_name',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Customer Name'
    )
    ->addColumn(
        'customer_addr_one',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Address 1'
    )
    ->addColumn(
        'customer_addr_two',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Address 2'
    )
    ->addColumn(
        'customer_addr_county',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Address County'
    )
    ->addColumn(
        'customer_addr_city',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Address City'
    )
    ->addColumn(
        'customer_addr_postcode',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Address Postcode'
    )
    ->addColumn(
        'customer_country',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => true
        ),
        'Address Country'
    )
    ->addColumn(
        'customer_telephone',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Customre Telephone'
    )
    ->addColumn(
        'customer_email_addr',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'nullable' => false
        ),
        'Customer Email Address'
    );

if (!$installer->getConnection()->isTableExists($failedTransactionsTable->getName())) {
    $installer->getConnection()->createTable($failedTransactionsTable);
}

$installer->endSetup();
