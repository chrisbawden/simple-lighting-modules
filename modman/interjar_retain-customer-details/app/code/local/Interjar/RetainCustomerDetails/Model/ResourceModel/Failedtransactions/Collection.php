<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_ResourceModel_Failedtransactions_Collection
 */
class Interjar_RetainCustomerDetails_Model_ResourceModel_Failedtransactions_Collection extends
    Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialise Resource
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('retaincustomerdetails/failedtransactions');
    }
}
