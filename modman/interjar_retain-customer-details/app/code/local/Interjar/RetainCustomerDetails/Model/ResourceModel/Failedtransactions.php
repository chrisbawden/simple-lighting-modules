<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_ResourceModel_Failedtransactions
 */
class Interjar_RetainCustomerDetails_Model_ResourceModel_Failedtransactions extends
    Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialise Table and Primary Key
     */
    protected function _construct()
    {
        $this->_init('retaincustomerdetails/failedtransactions', 'entity_id');
    }
}
