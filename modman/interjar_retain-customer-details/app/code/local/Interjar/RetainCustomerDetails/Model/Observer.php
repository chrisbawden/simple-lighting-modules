<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Model_Observer
 */
class Interjar_RetainCustomerDetails_Model_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function captureAndSaveFailedTransactions(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('retaincustomerdetails');
        if ($helper->getIsRetainCustomerDetailsModuleEnabled()) {
            $event = $observer->getEvent();
            $order = $event->getOrder();
            $quote = $event->getQuote();
            if (is_object($order) && is_object($quote)) {
                Mage::helper('retaincustomerdetails')->processFailedTransaction($order, $quote);
            } else {
                if ($helper->getIsDebugLoggingEnabled()) {
                    $helper->logFailedTransactionCapture();
                }
            }
        }
    }
}
