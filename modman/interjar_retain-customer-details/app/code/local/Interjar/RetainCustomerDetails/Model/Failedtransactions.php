<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Failedtransactions
 */
class Interjar_RetainCustomerDetails_Model_Failedtransactions extends Mage_Core_Model_Abstract
{
    /**
     * Initialise Resource Model
     */
    protected function _construct()
    {
        $this->_init('retaincustomerdetails/failedtransactions');
    }
}
