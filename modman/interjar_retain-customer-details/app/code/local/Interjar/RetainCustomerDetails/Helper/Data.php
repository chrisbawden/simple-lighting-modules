<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Helper_Data
 */
class Interjar_RetainCustomerDetails_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LOG_FILENAME = 'retaincustomerdetails.log';
    const RETAIN_CUSTOMER_DETAILS_MODULE_ENABLED = 'retaincustomerdetails/general/enable_retaincustomerdetails';
    const DEBUG_LOGGING_ENABLED = 'retaincustomerdetails/general/logging_enabled';
    const RETAIN_CUSTOMER_COOKIE_ADDRESS = 'retaincustomerdetails/general/retain_in_cookie';

    /**
     * Is Retain Customer Details module enabled?
     *
     * @return bool
     */
    public function getIsRetainCustomerDetailsModuleEnabled()
    {
        $isEnabled = Mage::getStoreConfigFlag(self::RETAIN_CUSTOMER_DETAILS_MODULE_ENABLED,
            Mage::app()->getStore()->getId());
        if ($isEnabled) {
            return true;
        }
        return false;
    }

    /**
     * Is Retain Customer Details in a cookie?
     *
     * @return bool
     */
    public function getRetainCustomerAddressInCookie()
    {
        $isCookie = Mage::getStoreConfigFlag(self::RETAIN_CUSTOMER_COOKIE_ADDRESS,
            Mage::app()->getStore()->getId());
        if ($isCookie) {
            return true;
        }
        return false;
    }

    /**
     * Is debug logging enabled?
     *
     * @return bool
     */
    public function getIsDebugLoggingEnabled()
    {
        $isLogging = Mage::getStoreConfigFlag(self::DEBUG_LOGGING_ENABLED,
            Mage::app()->getStore()->getId());
        if ($isLogging) {
            return true;
        }
        return false;
    }

    /**
     * Process the capture of a failed payment/order
     *
     * @param $order
     * @param $quote
     */
    public function processFailedTransaction($order, $quote)
    {
        if ($customer = $order->getCustomer()) {
            $billingAddress = $order->getBillingAddress();
            $this->saveFailedTransaction($order, $customer, $billingAddress);
            if (Mage::helper('retaincustomerdetails')->getRetainCustomerAddressInCookie()) {
                $this->saveCustomerAddressInformation($order, $customer, $billingAddress);
            }
        }
    }

    /**
     * Save failed transaction to database
     *
     * @param $customer
     * @param $billingAddress
     */
    private function saveFailedTransaction($order, $customer, $billingAddress)
    {
        $customerId = $customer->getId();
        $emailAddress = $billingAddress->getEmail();
        if (empty($emailAddress)) {
            $emailAddress = $order->getCustomer()->getEmail();
        }
        $customerAddress1 = $billingAddress->getStreet(1);
        $customerAddress2 = $billingAddress->getStreet(2);
        $customerTelephone = $billingAddress->getTelephone();
        $customerPostcode = $billingAddress->getPostcode();
        $customerCity = $billingAddress->getCity();
        $customerRegion= $billingAddress->getRegion();
        $customerCountry = $billingAddress->getCountryId();
        $customerName = $billingAddress->getName();
        try {
            $failedTransactions = Mage::getModel('retaincustomerdetails/failedtransactions');
            $failedTransactions->setCreatedAt((new DateTime())->format('Y-m-d H:i:s'));
            $failedTransactions->setCustomerId($customerId);
            $failedTransactions->setCustomerName($customerName);
            $failedTransactions->setCustomerEmailAddr($emailAddress);
            $failedTransactions->setCustomerAddrOne($customerAddress1);
            $failedTransactions->setCustomerAddrTwo($customerAddress2);
            $failedTransactions->setCustomerAddrCity($customerCity);
            $failedTransactions->setCustomerAddrCounty($customerRegion);
            $failedTransactions->setCustomerAddrPostcode($customerPostcode);
            $failedTransactions->setCustomerCountry($customerCountry);
            $failedTransactions->setCustomerTelephone($customerTelephone);
            $failedTransactions->save();
        } catch (Exception $exception) {
            Mage::logException($exception);
        }
    }

    /**
     * Save billing address to customers session
     *
     * @param $customer
     * @param $address
     */
    private function saveCustomerAddressInformation($order, $customer, $address)
    {
        $emailAddress = $address->getEmail();
        if (empty($emailAddress)) {
            $emailAddress = $order->getCustomer()->getEmail();
        }
        $customerAddress = array(
            'address_one' => $address->getStreet(1),
            'address_two' => $address->getStreet(2),
            'telephone' => $address->getTelephone(),
            'postcode' => $address->getPostcode(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'country_id' => $address->getCountryId(),
            'email_address' => $emailAddress
        );
        if ($customerId = $customer->getId()) {
            $customerAddress['customer_id'] = $customerId;
        }
        $cookieValue = serialize($customerAddress);
        Mage::getModel('core/cookie')->set('address_capture', $cookieValue);
    }

    /**
     * Log failed capture
     */
    public function logFailedTransactionCapture()
    {
        Mage::log('Failure to capture a failed service transaction.',
            Zend_Log::INFO, self::LOG_FILENAME);
    }
}
