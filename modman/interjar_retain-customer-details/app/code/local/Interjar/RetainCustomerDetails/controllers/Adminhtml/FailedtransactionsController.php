<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Adminhtml_FailedtransactionsController
 */
class Interjar_RetainCustomerDetails_Adminhtml_FailedtransactionsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Give index for failed transactions
     */
    public function indexAction()
    {
        $this->_title($this->__('Captured Failed Transactions'))
            ->_title($this->__('Captured Failed Transactions'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/sales');
        $this->_addContent($this->getLayout()
            ->createBlock('retaincustomerdetails/adminhtml_failedtransactions'));
        $this->renderLayout();
    }

    /**
     * Grid ajax action request action
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('retaincustomerdetails/adminhtml_failedtransactions_grid')->toHtml()
        );
    }

    /**
     * Retrieve adminhtml session model object
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('failedtransactions');
    }
}
