<?php
/**
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Block_Retaincustomer
 */
class Interjar_RetainCustomerDetails_Block_Checkout_Retaincustomer extends Mage_Core_Block_Template
{
    /**
     * Get address capture cookie for frontend
     *
     * @return mixed
     */
    public function getRetainerCookie()
    {
        $cookie = Mage::getModel('core/cookie')->get('address_capture');
        $cookieData = unserialize($cookie);
        if (!empty($cookieData)) {
            if (isset($cookieData['postcode'])) {
                return Mage::helper('core')->jsonEncode($cookieData);
            }
            return '';
        }
        return '';
    }

    /**
     * Return json data for frontend
     *
     * @return mixed
     */
    public function getAddressData()
    {
        $data = $this->getRetainerCookie();
        if ($data) {
            return $data;
        }
    }
}
