<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions
 */
class Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions constructor.
     */
    public function __construct()
    {
        $this->_blockGroup = 'retaincustomerdetails';
        $this->_controller = 'adminhtml_failedtransactions';
        $this->_headerText = Mage::helper('retaincustomerdetails')->__('Captured Failed Transactions');
        parent::__construct();
        $this->_removeButton('add');
    }
}