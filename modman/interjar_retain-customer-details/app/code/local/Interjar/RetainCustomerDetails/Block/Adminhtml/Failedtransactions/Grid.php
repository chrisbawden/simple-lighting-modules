<?php
/**
 * @package Interjar_RetainCustomerDetails
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */

/**
 * Class Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions_Grid
 */
class Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Interjar_RetainCustomerDetails_Block_Adminhtml_Failedtransactions_Grid constructor.
     * @throws Varien_Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('failedGrid');
        $this->setDefaultSort('entity_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Core_Helper_Abstract
     */
    public function _getHelper()
    {
        return Mage::helper('retaincustomerdetails');
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('retaincustomerdetails/failedtransactions')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header' => $this->_getHelper()->__('ID'),
                'width' => '50px',
                'index' => 'entity_id',
                'type' => 'number',
            ));

        $this->addColumn('created_at',
            array(
                'header' => $this->_getHelper()->__('Captured Date'),
                'width' => '150',
                'index' => 'created_at',
                'type' => 'date'
            ));

        $this->addColumn('customer_telephone',
            array(
                'header' => $this->_getHelper()->__('Telephone'),
                'width' => '150',
                'index' => 'customer_telephone',
                'type' => 'text'
            ));

        $this->addColumn('customer_email_addr',
            array(
                'header' => $this->_getHelper()->__('Email Address'),
                'width' => '150',
                'index' => 'customer_email_addr',
                'type' => 'text'
            ));

        $this->addColumn('customer_name',
            array(
                'header' => $this->_getHelper()->__('Customer Name'),
                'width' => '150',
                'index' => 'customer_name',
                'type' => 'text'
            ));

        $this->addColumn('customer_addr_one',
            array(
                'header' => $this->_getHelper()->__('Address 1'),
                'width' => '150',
                'index' => 'customer_addr_one',
                'type' => 'TEXT'
            ));

        $this->addColumn('customer_addr_two',
            array(
                'header' => $this->_getHelper()->__('Address 2'),
                'width' => '150',
                'index' => 'customer_addr_two',
                'type' => 'text'
            ));

        $this->addColumn('customer_addr_city',
            array(
                'header' => $this->_getHelper()->__('City'),
                'width' => '150',
                'index' => 'customer_addr_city',
                'type' => 'text'
            ));

        $this->addColumn('customer_addr_postcode',
            array(
                'header' => $this->_getHelper()->__('Postcode'),
                'width' => '150',
                'index' => 'customer_addr_postcode',
                'type' => 'text'
            ));

        $this->addColumn('customer_addr_county',
            array(
                'header' => $this->_getHelper()->__('County/Province'),
                'width' => '150',
                'index' => 'customer_addr_county',
                'type' => 'text'
            ));

        $this->addColumn('customer_country',
            array(
                'header' => $this->_getHelper()->__('Country'),
                'width' => '150',
                'index' => 'customer_country',
                'type' => 'text'
            ));

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}