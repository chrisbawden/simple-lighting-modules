<?php

class Craftyclicks_C2a_Model_Accent
{
	public function toOptionArray()
	{
		return array(
			array(
				'value' => 'default',
				'label' => 'default'
			),
			array(
				'value' => 'red',
				'label' => 'red'
			),
			array(
				'value' => 'pink',
				'label' => 'pink'
			),
			array(
				'value' => 'purple',
				'label' => 'purple'
			),
			array(
				'value' => 'deepPurple',
				'label' => 'deepPurple'
			),
			array(
				'value' => 'indigo',
				'label' => 'indigo'
			),
			array(
				'value' => 'blue',
				'label' => 'blue'
			),
			array(
				'value' => 'lightBlue',
				'label' => 'lightBlue'
			),
			array(
				'value' => 'cyan',
				'label' => 'cyan'
			),
			array(
				'value' => 'teal',
				'label' => 'teal'
			),
			array(
				'value' => 'green',
				'label' => 'green'
			),
			array(
				'value' => 'lightGreen',
				'label' => 'lightGreen'
			),
			array(
				'value' => 'lime',
				'label' => 'lime'
			),
			array(
				'value' => 'yellow',
				'label' => 'yellow'
			),
			array(
				'value' => 'amber',
				'label' => 'amber'
			),
			array(
				'value' => 'orange',
				'label' => 'orange'
			),
			array(
				'value' => 'deepOrange',
				'label' => 'deepOrange'
			),
			array(
				'value' => 'brown',
				'label' => 'brown'
			),
			array(
				'value' => 'grey',
				'label' => 'grey'
			),
			array(
				'value' => 'blueGrey',
				'label' => 'blueGrey'
			)
		);
	}
}
