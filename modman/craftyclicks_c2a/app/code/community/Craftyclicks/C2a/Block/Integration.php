<?php

class Craftyclicks_C2a_Block_Integration extends Mage_Core_Block_Template
{
	public function _construct() {
		parent::_construct();
	}
	public function addJS($param){
		$head = $this->getLayout()->getBlock('head');
		// If no head, probably called from ajax, just return
		if(!is_object($head)) return;

		$config = (object) array(
			'cc_pcl_frontend'	=> (int) Mage::getStoreConfig('clicktoaddress/general/active'),
			'cc_pcl_backend'	=> (int) Mage::getStoreConfig('clicktoaddress/general/active_admin_panel'),
			'cc_c2a_global'		=> (int) Mage::getStoreConfig('c2a_global/options/active')
		);

		if($config->cc_c2a_global){
			$path = 'craftyclicks/c2a/';

			$active_file = '';
			switch($param){
				case 'admin':
					$active_file = 'admin.js';
					break;
				case 'gomage':
					$active_file = 'gomage.js';
					break;
				default:
					$active_file = 'default.js';
					break;
			}
			/* detect checkouts that don't use unique layout tags */
			if(Mage::getStoreConfig('onestepcheckout/general/enable') == "1"){
				$active_file = 'advanced.js';
			}
			if(!empty($active_file))
				$head->addItem('js', $path.$active_file);
		}

		return $this;
	}
}
