document.observe('dom:loaded', function() {
	if($($('c2a_global_design_dropdown_accent'))){
		var colorcube = '<div id="color-cube"></div>';
		$('row_c2a_global_design_dropdown_accent').down('p.note').down('span').insert({after: colorcube});
		var colors = {
			default:	'#63a2f1',
			red:		'#F44336',
			pink:		'#E91E63',
			purple:		'#9C27B0',
			deepPurple:	'#673ab7',
			indigo:		'#3f51b5',
			blue:		'#2196f3',
			lightBlue:	'#03a9f4',
			cyan:		'#00bcd4',
			teal:		'#009688',
			green:		'#4caf50',
			lightGreen:	'#8bc34a',
			lime:		'#cddc39',
			yellow:		'#ffeb3b',
			amber:		'#ffc107',
			orange:		'#ff9800',
			deepOrange:	'#ff5722',
			brown:		'#795548',
			grey:		'#9e9e9e',
			blueGrey:	'#607d8b'
		};
		var currentColour = $('c2a_global_design_dropdown_accent').getValue();
		if(currentColour.length){
			$('color-cube').writeAttribute('data-value', currentColour);
			$('color-cube').setStyle({
				backgroundColor: colors[currentColour]
			});
		}
		$('c2a_global_design_dropdown_accent').on('change', function(event){
			var newColour = $(this).getValue();
			$('color-cube').writeAttribute('data-value', newColour);
			$('color-cube').setStyle({
				backgroundColor: colors[newColour]
			});
		});
	}

	if($('c2a_global_options_access_token')){
		$('c2a_global_options_access_token').insert({after:'<span id="cc_insert_token">Find My Access Token</span>'});
		$('cc_insert_token').observe('click', function(){
			get_cc_token(function(token){
				$('c2a_global_options_access_token').value = token;
			});
		});
	}
});
