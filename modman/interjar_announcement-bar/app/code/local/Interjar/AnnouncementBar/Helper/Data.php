<?php
/**
 * @package Interjar_AnnouncementBar
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */
class Interjar_AnnouncementBar_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ANNOUNCEMENT_BAR_ENABLED = 'announcementbar/general/enable_announcementbar';
    const ANNOUNCEMENT_BAR_CONTENT = 'announcementbar/general/announcements_content';

    /**
     * Is announcement bar enabled?
     * @return bool
     */
    public function getAnnouncementBarEnabled()
    {
        return Mage::getStoreConfigFlag(self::ANNOUNCEMENT_BAR_ENABLED,
            Mage::app()->getStore()->getId());
    }

    /**
     * Get announcement content
     *
     * @return mixed
     */
    public function getAnnouncementContent()
    {
        return Mage::getStoreConfig(self::ANNOUNCEMENT_BAR_CONTENT,
            Mage::app()->getStore()->getId());
    }
}
