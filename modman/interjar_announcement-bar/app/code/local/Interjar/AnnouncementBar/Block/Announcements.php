<?php
/**
 * @package Interjar_AnnouncementBar
 * @author Interjar Ltd
 * @author Andy Burns <andy@interjar.com>
 */
class Interjar_AnnouncementBar_Block_Announcements extends Mage_Core_Block_Template
{
    /** @var Interjar_AnnouncementBar_Helper_Data  */
    public $helper;

    /**
     * @return $this|Mage_Core_Block_Abstract
     */
    public function _beforeToHtml()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('interjar/announcementbar/announcements.phtml');
        }
        return $this;
    }

    /**
     * Interjar_AnnouncementBar_Block_Announcements constructor.
     */
    public function _construct()
    {
        $this->helper = Mage::helper('announcementbar');
    }

    /**
     * @return mixed
     */
    public function getAnnouncements()
    {
        if ($this->helper->getAnnouncementBarEnabled()) {
            $announcements = $this->helper->getAnnouncementContent();
            if (!empty($announcements)) {
                return $announcements;
            }
        }
    }
}
