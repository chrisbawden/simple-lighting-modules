# Webtise/Redis

A Magento 1.x module for redis

Uses the following packages:

- https://github.com/colinmollenhour/Cm_Cache_Backend_Redis
- https://github.com/colinmollenhour/Cm_RedisSession
- https://github.com/colinmollenhour/credis (Copied into lib/Credis)

This circumvents the base packages using submodules & abstract libraries aimed at Magento 2.
