<?php
/**
 * Created by PhpStorm.
 * User: joshuacarter
 * Date: 02/08/2018
 * Time: 10:12
 */
class Interjar_WcGaCookie_Helper_Data extends Webcooking_GoogleUniversalAnalytics_Helper_Data
{
    public function getClientId($createIfNotFound = false) {
        try {
            $coreSession = Mage::getSingleton('core/session');
        } catch(Exception $e) {
            return;
        }
        $guaClientId = $this->generateUuid();
        if((!$coreSession->getGuaClientId() && $createIfNotFound) ||
            ($coreSession->getGuaClientId() !== $guaClientId) && !$coreSession->getGuaAjaxSet()) {
            $coreSession->setGuaClientId($guaClientId);
            if ($coreSession->getGuaClientId()) { // If somehow $coreSession->getGuaClientId() return empty, there is risks of infinite loop when sending the event
                if (!Mage::registry('GUA_NEW_CLIENT_ID_NEED_SESSION_START')) {
                    Mage::register('GUA_NEW_CLIENT_ID_NEED_SESSION_START', true);
                }
            }
        }
        return $coreSession->getGuaClientId();
    }
}
