<?php
/**
 * Created by PhpStorm.
 * User: joshuacarter
 * Date: 02/08/2018
 * Time: 10:12
 */
class Interjar_WcGaCookie_AjaxController extends Mage_Core_Controller_Front_Action
{
    /**
     * Issues with retrieving _ga cookie through PHP
     * $_COOKIE so this workaround was put in place
     */
    public function indexAction()
    {
        $cookieValue = $this->getRequest()->getPost('val');
        if ($cookieValue) {
            $cookieValue = urlencode($cookieValue);
            $coreSession = Mage::getSingleton('core/session');
            if (!$coreSession->getGuaClientId() ||
                ($coreSession->getGuaClientId() != $cookieValue)) {
                $guaClientId = preg_replace('%GA1\.[0-9]+\.%', '', $cookieValue);
                $coreSession->setGuaClientId($guaClientId);
                $coreSession->setGuaAjaxSet(true);
            }
        }
    }
}
