<?php
/**
 * Created by PhpStorm.
 * User: joshuacarter
 * Date: 02/08/2018
 * Time: 13:07
 */
class Interjar_WcGaCookie_Model_Observer extends Webcooking_GoogleUniversalAnalytics_Model_Observer
{
    /**
     * @param $observer
     * @interjar rewrite to only save client id to order if not already set
     */
    public function observeSalesOrderSaveBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        if (!$order->getGuaClientId()) {
            Mage::helper('googleuniversalanalytics')->saveGuaClientIdToOrder($order);
        }
    }
}
