<?php class Webtise_FoundationAjaxCart_Model_Observer
{
    /**
     *
     */
    public function coreBlockAbstractToHtmlBefore(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        $request = $block->getRequest();
        $helper = Mage::helper('webtise_foundationajaxcart');

        if($block instanceof Mage_Catalog_Block_Product_View && $block->getNameInLayout() == "product.info")
        {
            $cartUpdatePath = 'checkout' . DS . 'cart' . DS . 'configure';
            $routeName = $request->getRouteName();
            $controllerName = $request->getControllerName();
            $actionName = $request->getActionName();
            $actionPath = $routeName . DS . $controllerName . DS . $actionName;

            if($actionPath == $cartUpdatePath)
            {
                $id = Mage::app()->getFrontController()->getRequest()->getParam('id');
                $block->setSubmitRouteData(array(
                    'route' => $helper->getAjaxRoute('update'),
                    'params' => array('id' => $id)
                ));
            }else{
                $block->setSubmitRouteData(array(
                    'route' => $helper->getAjaxRoute()
                ));
            }
        }

    }
}