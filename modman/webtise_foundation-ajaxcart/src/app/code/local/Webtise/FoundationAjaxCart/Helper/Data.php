<?php
class Webtise_FoundationAjaxCart_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get route required for ajax urls by action type
     *
     * @param  string $type 'update' || 'delete' || 'add' (default)
     * @return string   Route for ajax request
     */
    public function getAjaxRoute($type='add')
    {
        $route = 'webtise_ajaxcart' . DS . 'index' . DS;

        if( $type === "update" )
        {
            $route .= 'updateItemOptions';
        }
        else if( $type === "delete" )
        {
            $route .= 'ajaxDelete';
        }
        else
        {
            $route .= 'index';
        }

        return $route;
    }

    /**
     * Get item ajax delete url
     *
     * @return string
     */
    public function getAjaxDeleteUrl($id)
    {
        $route = self::getAjaxRoute('delete');
        $encodedParamName = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
        $params = array(
            'id' => $id,
             $encodedParamName => Mage::helper('core/url')->getEncodedUrl(),
            '_secure' => Mage::app()->getStore()->isCurrentlySecure(),
        );

        return Mage::getUrl($route, $params);
    }
}