<?php
class Webtise_FoundationAjaxCart_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Initialize product instance from request data
     *
     * @return Mage_Catalog_Model_Product || false
     */
    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }

    public function indexAction(){

        $cart = Mage::getSingleton('checkout/cart');
        $session = Mage::getSingleton('checkout/session');
        $params = $this->getRequest()->getParams();

        try{
            $product = $this->_initProduct();
            $cart->addProduct($product,$params);
            $cart->save();
            $session->setCartWasUpdated(true);

            if (!$cart->getQuote()->getHasError()) {
                $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                $session->addSuccess($message);
            }

            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

        }catch (Mage_Core_Exception $e) {
            $session->addException($e, $e->getMessage());
            Mage::logException($e);
        }catch (Exception $e) {
            $session->addException($e, $e->getMessage());
            Mage::logException($e);
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    public function updateItemOptionsAction()
    {
        $cart = Mage::getSingleton('checkout/cart');
        $session = Mage::getSingleton('checkout/session');
        $id = (int) $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();
        $updatedItemId;

        if (!isset($params['options'])) {
            $params['options'] = array();
        }
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                Mage::throwException($this->__('Quote item is not found.'));
            }

            $item = $cart->updateItem($id, new Varien_Object($params));


            if (is_string($item)) {
                Mage::throwException($item);
            }
            if ($item->getHasError()) {
                Mage::throwException($item->getMessage());
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $session->setCartWasUpdated(true);

            Mage::dispatchEvent('checkout_cart_update_item_complete',
                array('item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
//            if (!$session->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->escapeHtml($item->getProduct()->getName()));
                    $session->addSuccess($message);
                    $updatedItemId = $item->getItemId();
                }
//            }
        } catch (Mage_Core_Exception $e) {
            if ($session->getUseNotice(true)) {
                $session->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $session->addError($message);
                }
            }
        } catch (Exception $e) {
            $session->addException($e, $this->__('Cannot update the item.'));
            Mage::logException($e);
        }

        $this->loadLayout();

        $layout = $this->getLayout();
        $block = $layout->getBlock('webtise.foundation.ajaxcart.json');
        $block->setUpdatedItemId($updatedItemId);

        $this->renderLayout();

    }

    /**
     * Minicart delete action
     */
    public function ajaxDeleteAction()
    {
//        if (!$this->_validateFormKey()) {
//            Mage::throwException('Invalid form key');
//        }
        $id = (int) $this->getRequest()->getParam('id');
        $cart = Mage::getSingleton('checkout/cart');
        $session = Mage::getSingleton('checkout/session');

        if ($id) {
            try {
                $cart->removeItem($id)->save();
                $message = $this->__('Item was removed successfully.');
                $session->addSuccess($message);
            } catch (Exception $e) {
                $session->addException($e, $e->getMessage());
                Mage::logException($e);
            }
        }

        $this->loadLayout();
        $this->renderLayout();
    }

}