/*jshint -W030 */ // TODO - This prevents linter throwing a hissy fit, figure out why
!function($, Foundation) {
    'use strict';

    function Ajaxcart (element, options) {
        this.$element = element;
        this.options = $.extend({}, Ajaxcart.defaults, this.$element.data(), options);

        this._init();
        this._events();
        this._alterDom();
        Foundation.registerPlugin(this, 'Ajaxcart');
    }

    Ajaxcart.prototype._init = function () {
        var _this = this;
        this.ajaxUrl = {
            add: /\/webtise_ajaxcart\/index\/index\/$/,
            remove: '\/webtise_ajaxcart\/index\/ajaxDelete\/id/',
            update: '\/webtise_ajaxcart\/index\/updateItemOptions\/id/'
        };
        this.$ajaxMessages = $(this.options.ajaxMessagesSelector);
        this.$form = $(this.options.addToCartFormSelector);
        this.$topLink = $(this.options.topLinkSelector);
        this.$summary = $(this.options.summarySelector);
        this.formValid = this._validateForm(this.$form);
        this.$blockCart = this.$element.find(this.options.blockCartSelector);
        this.$blockCartMessages = $('<div data-ajaxcart-messages />');
        this.$offcanvas = this.$element.parents(this.options.offcanvasSelector);

    };

    Ajaxcart.prototype._events = function () {
        // Handle form submission events
        if( this.formValid ) {
            // Cache form submit button
            this.$formSubmitButton = this.$form.find(this.options.addToCartFormButtonSelector);
            this._handleFormSubmit(this.$form);
        }
        // Handle remove links
        this._handleRemoveLinks();
        // Handle change event
        this._handleCartUpdated();

    };

    Ajaxcart.prototype._alterDom = function() {
        // Add element to recieve the block cart's ajax messages
        this.$element.prepend(this.$blockCartMessages);
    };

    Ajaxcart.prototype._validateForm = function ($form) {
        // Check for single form element
        if( this.$form.length === 1 ) {

            var action = $form.attr('action'),
                ajaxAddUrl = action.match(this.ajaxUrl.add),
                ajaxUpdateUrl = action.match(this.ajaxUrl.update);

            // If action is AJAX cart route
            if( ajaxAddUrl || ajaxUpdateUrl ) {
                return ajaxAddUrl || ajaxUpdateUrl;
            }

        } else if ( this.$form.length > 1 ) {

            console.warn(this.options.addToCartFormSelector + ' matched ' + this.$form.length + ' elements on this page. It should only match 1.');

        }

        return false;
    };

    Ajaxcart.prototype._handleFormSubmit = function ($form) {
        var _this = this;
        // Set form submit event handler
        this.$form.on('submit.zf.ajaxcart', function (event) {
            // Prepare DOM
            _this._beforeFormSubmit();
            // Process AJAX request
            _this._ajaxFormSubmit($form.attr('action'), $form.serializeArray());
            // Prevent form submitting
            return false;
        });
    };

    Ajaxcart.prototype._beforeFormSubmit = function () {
        // Disable form submit button
        this.$formSubmitButton
            .prop('disabled', true)
            .addClass('loading');
        // Remove AJAX messages
        this.$ajaxMessages
            .empty();
        // Remove any previous $blockCart messages
        this.$blockCart
            .find('.callout')
            .remove();
    };

    Ajaxcart.prototype._ajaxFormSubmit = function (url, formData) {
        var _this = this;
        // Send AJAX Request
        $.ajax(url, {
            cache: false,
            data: formData,
            method: 'POST'
        }).done(function (data) { // Successful AJAX request
            // Process returned data
            _this._ajaxDone(data);
        }).fail(function (jqXHR, status) { // Failed AJAX request
            // Process returned data
            _this._ajaxFail(status);
        }).always(function () { // After all AJAX requests
            // Re-enable button
            _this.$formSubmitButton
                .prop('disabled', false)
                .removeClass('loading');
        });

    };

    Ajaxcart.prototype._ajaxDone = function (data) {
        // Parse Data
        var response = $.parseJSON(data);
        // If message type is success
        if( response.last_message_type === "success" ) { // Successfully added to / removed from cart
            // Update topLink
            this.$topLink.replaceWith(response.toplink);
            // Re-select new topLink (replaceWith returns removed object)
            this.$topLink = $(this.options.topLinkSelector);
            // Update summary
            this.$summary.replaceWith(response.summary);
            // Re-select new summary (replaceWith returns removed object)
            this.$summary = $(this.options.summarySelector);
            // Update blockCart contents
            this.$blockCart.replaceWith(response.sidebar);
            // Re-select new blockCart (replaceWith returns removed object)
            this.$blockCart = this.$element.find(this.options.blockCartSelector);
            // Update cart messages
            this.$blockCartMessages.html(response.messages);
            // Trigger change event
            this.$element.trigger('change.zf.ajaxcart');
        // If message type is error
        } else if( response.last_message_type === "error" ) { // Failed to add to / remove from cart
            // Update AJAX messages
            $(response.messages).appendTo(this.$ajaxMessages);
        }
    };

    Ajaxcart.prototype._ajaxFail = function (status) {
        // Print error to console
        console.error('Ajax cart error' + status);
        return false;
    };

    Ajaxcart.prototype._handleRemoveLinks = function () {
        var _this = this;
        // Set remove link event handler
        this.$element.on('click.zf.ajaxcart', this.options.removeLinkSelector, function (event) {

            var $link = $(this),
                action = $link.attr('href'),
                hasValidAjaxRemoveUrl = action.match(_this.ajaxUrl.remove);
            // Check is ajaxDelete url
            if(! hasValidAjaxRemoveUrl ) {
                return true;
            }
            // Prevent multiple requests
            if( $link.hasClass('loading') ) {
                return false;
            }
            // Prepare DOM
            $link.addClass('loading');
            _this.$element.addClass('loading');
            // Process Ajax Request
            _this._ajaxRemoveLink($link, action);
            // Prevent link being followed
            return false;
        });

    };

    Ajaxcart.prototype._ajaxRemoveLink = function ($link, url) {
        var _this = this;
        // Send AJAX Request
        $.ajax(url, {
            cache: false,
            method: 'POST'
        }).done(function (data) { // Successful AJAX request
            // Process returned data
            _this._ajaxDone(data);
        }).fail(function (jqXHR, status) { // Failed AJAX request
            // Process returned data
            _this.ajaxFail(status);
        }).always(function () { // After all AJAX requests
            // Remove loading classes
            $link.removeClass('loading');
            _this.$element.removeClass('loading');

        });

    };

    Ajaxcart.prototype._handleCartUpdated = function () {
        var _this = this;
        // Reveal off canvas on cart update if config true and cart in off canvas
        if( this.options.revealOnUpdate && this.$offcanvas.length > 0) {
            this.$element.on('change.zf.ajaxcart', function() {
                _this.$offcanvas.foundation('open');
            });
        }
    };

    Ajaxcart.prototype.destroy = function () {
        this.$form.off('.zf.ajaxcart');
        this.$element.off('.zf.ajaxcart');
        Foundation.unregisterPlugin(this);
    };

    Ajaxcart.defaults = {
        addToCartFormButtonSelector: '[type=submit]',
        addToCartFormSelector: '#product_addtocart_form',
        ajaxMessagesSelector: '#ajax_messages',
        blockCartSelector: '.block-cart',
        offcanvasSelector: '[data-off-canvas]',
        removeLinkSelector: '.btn-remove',
        revealOnUpdate: true,
        topLinkSelector: '.top-link-cart',
        summarySelector: '.cart-summary'
    };

    Foundation.plugin(Ajaxcart, 'Ajaxcart');

}(jQuery, window.Foundation);
