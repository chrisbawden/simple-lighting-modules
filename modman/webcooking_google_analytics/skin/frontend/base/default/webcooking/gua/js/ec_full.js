/**
 * Copyright (c) 2011-2017 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
function sendGuaProductVariantDetails(productConfig) {
    if(!productDetail) {
        return;
    }
    var variant = [];
    var price = parseFloat(productConfig.config.basePrice);
    for(attributeId in productConfig.config.attributes) {
        stateValue = productConfig.state[attributeId];
        for(var i=0; i < productConfig.config.attributes[attributeId].options.length; i++) {
            if(productConfig.config.attributes[attributeId].options[i].id == stateValue) {
                variant.push(productConfig.config.attributes[attributeId].options[i].label);
                price += parseFloat(productConfig.config.attributes[attributeId].options[i].price);
            }
        }
    }
    productDetail.variant = variant.join(' - ');
    productDetail.price = price;
    
    gtag('event', 'view_item', {
       'items': [
           productDetail
       ]
    });
}
function guaOnProductClick(event) {
    var productField = {};
    productField['id'] = $(this).getAttribute('data-gua-ec-id');
    productField['name'] = $(this).getAttribute('data-gua-ec-name');
    productField['category'] = $(this).getAttribute('data-gua-ec-category');
    productField['brand'] = $(this).getAttribute('data-gua-ec-brand');
    productField['variant'] = $(this).getAttribute('data-gua-ec-variant');
    productField['position'] = $(this).getAttribute('data-gua-ec-position');
    productField['price'] = $(this).getAttribute('data-gua-ec-price');
    productField['list'] = $(this).getAttribute('data-gua-ec-list');
    productField['noredirect'] = $(this).getAttribute('data-gua-ec-noredirect');
    if(productField['id'] || productField['name']) {
        var href = $(this).getAttribute('href');
        if(!productField['list']) {
            productField['list'] = 'Product list';
        }
        
        
        gtag('event', 'select_content', {
            "content_type": "product",
            "items": [
              productField
            ],
            "event_callback": function() {
                    if(productField['noredirect']) {
                        return;
                    }
                    document.location = href;
                }
        });
        
        
        
        
        
        
        
        if(ga.loaded) {
            setTimeout(function(){ document.location = href; }, 500); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}

function guaOnPromoClick(event) {
    if(wcIsGtm) {
        return gtmOnPromoClick(event);
    }
    var promoField = {};
    promoField['id'] = $(this).getAttribute('data-gua-ec-promo-id');
    promoField['name'] = $(this).getAttribute('data-gua-ec-promo-name');
    promoField['creative'] = $(this).getAttribute('data-gua-ec-promo-creative');
    promoField['position'] = $(this).getAttribute('data-gua-ec-promo-position');
    promoField['noredirect'] = $(this).getAttribute('data-gua-ec-promo-noredirect');
    if(promoField['id'] || promoField['name']) {
        var href = $(this).getAttribute('href');
        
        gtag('event', 'select_content', {
            "promotions": [
              promoField
            ],
            "event_callback": function() {
                    if(promoField['noredirect']) {
                        return;
                    }
                    document.location = href;
                }
            
        });
        
        if(ga.loaded) {
            setTimeout(function(){ document.location = href; }, 500); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}
function sendGtmProductVariantDetails(productConfig) {
    if(!productDetail) {
        return;
    }
    var variant = [];
    var price = parseFloat(productConfig.config.basePrice);
    for(attributeId in productConfig.config.attributes) {
        stateValue = productConfig.state[attributeId];
        for(var i=0; i < productConfig.config.attributes[attributeId].options.length; i++) {
            if(productConfig.config.attributes[attributeId].options[i].id == stateValue) {
                variant.push(productConfig.config.attributes[attributeId].options[i].label);
                price += parseFloat(productConfig.config.attributes[attributeId].options[i].price);
            }
        }
    }
    productDetail.variant = variant.join(' - ');
    productDetail.price = price;
    dataLayer.push({
        'ecommerce': {
          'detail': {
            'products': [productDetail]
           }
         }
    });
}
function gtmOnProductClick(event) {
    var productField = {};
    productField['id'] = $(this).getAttribute('data-gtm-ec-id');
    productField['name'] = $(this).getAttribute('data-gtm-ec-name');
    productField['category'] = $(this).getAttribute('data-gtm-ec-category');
    productField['brand'] = $(this).getAttribute('data-gtm-ec-brand');
    productField['variant'] = $(this).getAttribute('data-gtm-ec-variant');
    productField['position'] = $(this).getAttribute('data-gtm-ec-position');
    productField['price'] = $(this).getAttribute('data-gtm-ec-price');
    productField['noredirect'] = $(this).getAttribute('data-gtm-ec-noredirect');
    if(productField['id'] || productField['name']) {
        var href = $(this).getAttribute('href');
        var listValue = $(this).getAttribute('data-gtm-ec-list');
        if(!listValue) {
            listValue = 'Product list';
        }
        dataLayer.push({
            'event':'GAevent',
            'eventCategory':'click',
            'eventAction':'product',
            'eventLabel': productField['id'] + ' ' + productField['name'],
            'ecommerce': {
              'click': {
                'actionField': {'list': listValue},      // Optional list property.
                'products': [productField]
               }
             },
             'eventCallback': function() {
                if(productField['noredirect']) {
                    return;
                }
                document.location = href;
             }
        });
        if(ga.loaded) {
            setTimeout(function(){ document.location = href; }, 500); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}

function gtmOnPromoClick(event) {
    var promoField = {};
    promoField['id'] = $(this).getAttribute('data-gtm-ec-promo-id');
    promoField['name'] = $(this).getAttribute('data-gtm-ec-promo-name');
    promoField['creative'] = $(this).getAttribute('data-gtm-ec-promo-creative');
    promoField['position'] = $(this).getAttribute('data-gtm-ec-promo-position');
    promoField['noredirect'] = $(this).getAttribute('data-gtm-ec-promo-noredirect');
    if(promoField['id'] || promoField['name']) {
        var href = $(this).getAttribute('href');
        dataLayer.push({
            'event':'GAevent',
            'eventCategory':'click',
            'eventAction':'promo',
            'eventLabel': promoField['id'] + ' ' + promoField['name'],
            'ecommerce': {
              'promoClick': {
                'promotions': [promoField]
              }
            },
            'eventCallback': function() {
                if(promoField['noredirect']) {
                    return;
                }
              document.location = href;
            }
        });
        if(ga.loaded) {
            setTimeout(function(){ document.location = href; }, 500); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}
Event.observe(window, 'load', function() {
    $$('a[data-gua-ec-id]').invoke('observe', 'click', guaOnProductClick);
    $$('a[data-gua-ec-promo-id]').invoke('observe', 'click', guaOnPromoClick);
    $$('a[data-gtm-ec-id]').invoke('observe', 'click', gtmOnProductClick);
    $$('a[data-gtm-ec-promo-id]').invoke('observe', 'click', gtmOnPromoClick);
});