<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Observer {

    protected $_protocol = null;
    protected $_enhancedEcommerceHelper = null;
    
    public function getMeasurementProtocol() {
        if(is_null($this->_protocol)) {
            $this->_protocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        }
        return $this->_protocol;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    
    public function observeOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $this->_refundCanceledOrder($order);
        
    }
    
    public function observeCatalogProductCompareAddProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_add_product')) {
            $this->_sendEvent('compare', 'add', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeCatalogProductCompareRemoveProduct($observer) {
        $product = $observer->getEvent()->getProduct();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/compare_remove_product')) {
            $this->_sendEvent('compare', 'remove', 'Product #' . $product->getSku() . ' added to list', 1);
        }
    }
    
    public function observeControllerActionLayoutGenerateBlocksAfter($observer) {
        // deprecateed
        /*$layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive() && $layout->getBlock('after_body_start')) {
            $blocksToMove = array('google_universal_analytics_init','google_universal_analytics_gtm');
            foreach($blocksToMove as $blockName) {
                $block = $layout->getBlock($blockName);
                if($block) {
                    $afterBodyStartBlock = $layout->getBlock('after_body_start');
                    $headBlock = $layout->getBlock('head');
                    $headBlock->unsetChild($block->getNameInLayout());
                    $afterBodyStartBlock->append($block);
                    $block->setParentBlock($afterBodyStartBlock);
                }
            }
        }*/
        
    }
    
    
    public function observeControllerActionPredispatch($observer) {
        $coreSession = Mage::getSingleton('core/session');
        $cookieName = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_name');
        $cookieName = $cookieName ? $cookieName : '_ga';
        $cookieValue = Mage::getModel('core/cookie')->get($cookieName);
        if($cookieValue && $coreSession->getGuaClientId() != $cookieValue) {
            $guaClientId = preg_replace('%GA1\.[0-9]+\.%', '', $cookieValue);
            $coreSession->setGuaClientId($guaClientId);
            $coreSession->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
        }
        if(!$coreSession->getGuaClientId()) {
            return;
        }
        
        $fullActionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/checkout_onepage')) {
            if (in_array($fullActionName, array('checkout_onepage_saveBilling', 'firecheckout_index_saveBilling'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveBilling');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShipping', 'firecheckout_index_saveShipping'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShipping');
            } else if (in_array($fullActionName, array('checkout_onepage_savePayment'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/savePayment');
            } else if (in_array($fullActionName, array('checkout_onepage_saveShippingMethod'))) {
                $this->getMeasurementProtocol()->sendPageView('/checkout/onepage/saveShippingMethod');
            }
        }
        
        if ($fullActionName == 'contacts_index_post' && Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/contacts_submitted')) {
            $this->getMeasurementProtocol()->sendPageView('/contacts/submitted');
        }
        if ($fullActionName == 'solrsearch_ajax_suggest' && Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/solrsearch_ajax_suggest')) {
            $this->getMeasurementProtocol()->sendPageView('/solrsearch/ajax/suggest', array(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_TITLE =>Mage::helper('solrsearch')->__("Search results for: '%s'", Mage::helper('solrsearch')->getQueryText())));
        }
        if ($fullActionName == 'checkout_cart_index') {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCart($cart);
        } else if (in_array($fullActionName, array('checkout_onepage_index', 'onestepcheckout_index_index', 'onepagecheckout_index_index', 'firecheckout_index_index', 'singlepagecheckout_index_index', 'onepage_index_index', 'opc_index_index', 'speedycheckout_index_index'))) {
            $cart = Mage::getSingleton('checkout/cart');
            $this->_sendCheckoutCheckout($cart);
        } else if ($fullActionName == 'checkout_cart_estimatePost') {
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/checkout_estimate')) {
                $this->_sendEvent('checkout', 'estimate', Mage::app()->getRequest()->getParam('country_id').'.'.Mage::app()->getRequest()->getParam('estimate_postcode'), 1);
            }
        }
       
    }
    

    public function observeWishlistProductAddAfter($observer) {
        $items = $observer->getEvent()->getItems();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_add_product')) {
            $productList = array();
            foreach ($items as $item) {
                $productList[] = $item->getProductId();
            }
            $this->_sendEvent('wishlist', 'add_product', 'Products : ' . implode(', ', $productList) . ' added to wishlist', 1);
        }
    }

    public function observeWishlistShare($observer) {
        $wishlist = $observer->getEvent()->getWishlist();
        if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/wishlist_share')) {
            $value = 0;
            foreach($wishlist->getItemCollection() as $wishlistItem) {
                $value += $wishlistItem->getPrice()*max(1, $wishlistItem->getQty());
            }
            $this->_sendEvent('wishlist', 'share', 'Customer #' . $wishlist->getCustomerId() . ' shared wishlist ', $value);
        }
    }

    public function observeSalesruleValidatorProcess($observer) {
        if(Mage::registry('prevent_observe_salesrule_validator_process')) {
           return; 
        } 
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        if ($rule->getCouponCode() && Mage::getStoreConfig('googleuniversalanalytics/events/checkout_add_coupon')) {
            $this->_sendEvent('checkout', 'add_coupon', 'Add coupon code : ' . $rule->getCouponCode(), 1);
        }
    }

    public function observeNewsletterSubscriberSaveCommitAfter($observer) {
        $subscriber = $observer->getEvent()->getDataObject();
        $statusChange = $subscriber->getIsStatusChanged();
        if ($subscriber->getSubscriberStatus() == "1" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_subscribe')) {
                $this->_sendEvent('newsletter', 'subscribe', 'New newsletter subscriber : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_subscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/subscribe');
            }
        }
        if ($subscriber->getSubscriberStatus() == "0" && $statusChange) {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/events/newsletter_unsubscribe')) {
                $this->_sendEvent('newsletter', 'unsubscribe', 'Newsletter unsubscribed : ' . $subscriber->getId(), 1);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/newsletter_unsubscribe')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/newsletter/unsubscribe');
            }
        }
    }

    public function observeSalesQuoteRemoveItem($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $quoteItem->getProduct();
        if (!Mage::getStoreConfig('googleuniversalanalytics/events/checkout_remove_product') || !Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable() || Mage::app()->getStore()->isAdmin()) {
            return;
        }
        
        
        if($quoteItem->getParentItemId() && $quoteItem->getParentItem() && $quoteItem->getParentItem()->getProductType() == "configurable") {
            $quoteItem = $quoteItem->getParentItem();
        }
        
        $itemPrice = $this->getEcHelper()->getItemPriceForQuote($quoteItem);
        $params = array();
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_name')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = Mage::helper('googleuniversalanalytics')->formatData($quoteItem->getName());
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_sku')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getSku();
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_id')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getProductId();
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_cost')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getBaseCost();
        }
        if ($idx = Mage::getStoreConfig('googleuniversalanalytics/dimensions/product_profit')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $quoteItem->getPriceInclTax() - $quoteItem->getBaseCost();
        }
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = $itemPrice;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'remove';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $this->_sendEvent('checkout', 'remove_product', $this->getEcHelper()->getProductSkuValue($product), $itemPrice, false, false, null, $params);
        
    }

    public function observeCheckoutCartProductAddAfter($observer) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable() || Mage::app()->getStore()->isAdmin()) {
            return;
        }
        
        if($quoteItem->getParentItemId() && $quoteItem->getParentItem() && $quoteItem->getParentItem()->getProductType() == "configurable") {
            $quoteItem = $quoteItem->getParentItem();
        }
        
        
        $itemPrice = $this->getEcHelper()->getItemPriceForQuote($quoteItem);
        $params = array();
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensionsForProduct($product);
        foreach($customDimensions as $idx=>$value) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $idx] = $value;
        }
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, 1)] = $this->getEcHelper()->getProductSkuValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, 1)] = $this->getEcHelper()->getProductNameValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, 1)] = $this->getEcHelper()->getProductCategoryValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, 1)] = $this->getEcHelper()->getProductBrandValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, 1)] = $this->getEcHelper()->getProductVariantValue($product);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, 1)] = $itemPrice;
        $requestParams = Mage::app()->getRequest()->getParams();
        if(is_array($requestParams) && isset($requestParams['product']) && isset($requestParams['qty']) && $requestParams['product'] == $product->getId()) {
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, 1)] = max(1, intval($requestParams['qty']));
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'add';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $this->_sendEvent('checkout', 'add_product', $this->getEcHelper()->getProductSkuValue($product), $itemPrice, false, false, null, $params);
        
    }

    public function observeCustomerLogin($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_login')) {
            $this->_sendEvent('customer', 'login', 'Customer login #' . $customer->getId(), 1);
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/customer_login_success')) {
            $this->getMeasurementProtocol()->sendPageView('/customer/login/success');
        }
        $this->_sendCheckoutLogin($customer);
    }

    public function observeCustomerLogout($observer) {
        $customer = $observer->getEvent()->getCustomer();
        Mage::getSingleton('core/session')->setGuaCustomerLogout(1);
        if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_logout')) {
            $this->_sendEvent('customer', 'logout', 'Customer logout #' . $customer->getId(), 1);
        }
    }

    public function /*observeCustomerRegisterSuccess*/observeCustomerSaveAfter($observer) {
        $customer = $observer->getEvent()->getCustomer();
        if(!$customer->getOrigData('entity_id') && !$customer->getData('gua_registration_sent')) { // registration
            $customer->setData('gua_registration_sent', 1);/*prevent double event treatment */
            if (Mage::getStoreConfig('googleuniversalanalytics/events/customer_register')) {
                $this->_sendEvent('customer', 'registration', 'Customer registration #' . $customer->getId(), 1);
            }
            if (Mage::getStoreConfig('googleuniversalanalytics/extra_pageviews/customer_registration')) {
                $this->getMeasurementProtocol()->sendPageView('/customer/register/success');
            }
        }
    }

    public function observeCheckoutMultishippingControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeCheckoutOnepageControllerSuccessAction($observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        $this->_setGuaOnSuccessPageView($orderIds);
    }

    public function observeSalesOrderSaveBefore($observer) {
        $order = $observer->getEvent()->getOrder();
        Mage::helper('googleuniversalanalytics')->saveGuaClientIdToOrder($order);
    }
    
    public function observeSalesQuoteSaveBefore($observer) {
        $quote = $observer->getEvent()->getQuote();
        $this->_saveGuaDataToQuote($quote);
    }
    
    public function observeSalesOrderSaveAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        if($order->getOrigData('status') == $order->getData('status')) {
            return;
        }
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS && in_array($order->getData('status'), explode(',', Mage::getStoreConfig('googleuniversalanalytics/transactions/order_status_changed', $order->getStoreId())));
        $shouldSendTransaction = $shouldSendTransaction || Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId())  == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_ORDER;
        $shouldSendTransaction = $shouldSendTransaction && !$order->getGuaSentFlag();
        try {
            if ($shouldSendTransaction) {
                $this->getMeasurementProtocol()->sendTransactionRequestFromOrder($order);
            }
            if (Mage::getStoreConfigFlag('googleuniversalanalytics/events/order_status')) {
                $this->_sendEvent('sales', 'order_status', 'Order : ' . Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order) . ' has new status : ' . $order->getStatus(), 1, Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPlaceAfter($observer) {
       $order = $observer->getEvent()->getOrder();
       $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER;
        
        try {
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_created')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/created', array(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId());
            }
            if ($shouldSendTransaction) {
                $this->getMeasurementProtocol()->sendTransactionRequestFromOrder($order);
            }
            if (!$shouldSendTransaction) { //TODO : Make this optionnal
                $this->_sendEvent('sales', 'order_placed', 'Order : ' . Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order) . ' has been placed', $order->getBaseGrandTotal(), Mage::app()->getStore()->isAdmin(), $order->getGuaClientId(),$order->getStoreId());
            }
            Mage::getSingleton('checkout/session')->setGuaCheckoutStep(0);
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderCreditmemoRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();
        try {
            $this->_sendRefundRequestFromCreditmemo($creditmemo, $order);
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_refunded')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/refunded', array(),  true, $order->getGuaClientId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoicePay($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PAY_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->getMeasurementProtocol()->sendTransactionRequestFromInvoice($invoice, $order);
            }
            if (!$shouldSendTransaction) {///TODO : Make this optionnal
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order) . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }
    
    public function observeSalesOrderInvoiceSaveAfter($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE;
        //Mage::helper('googleuniversalanalytics')->log(Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()), null, 'gua.debug.log');
        try {
            if ($shouldSendTransaction) {
               $this->getMeasurementProtocol()->sendTransactionRequestFromInvoice($invoice, $order);
            }
            if(Mage::getStoreConfigFlag('googleuniversalanalytics/extra_pageviews/order_invoiced')) {
                $this->getMeasurementProtocol()->sendPageView('/gua/order/invoiced', array(),  true, $order->getGuaClientId());
            }
            if (!$shouldSendTransaction) {//TODO : Make this optionnal
                $this->_sendEvent('sales', 'order_invoiced', 'Order : ' . Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order) . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    public function observeSalesOrderPaymentPay($observer) {
        $payment = $observer->getEvent()->getPayment();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $shouldSendTransaction = Mage::getStoreConfig('googleuniversalanalytics/transactions/transaction_event', $order->getStoreId()) == Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE;
        try {
            if ($shouldSendTransaction) {
               $this->getMeasurementProtocol()->sendTransactionRequestFromInvoice($invoice, $order);
            }
            if (!$shouldSendTransaction) {//TODO : Make this optionnal
                $this->_sendEvent('sales', 'order_paid', 'Order : ' . Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order) . ' has been paid', 1, true, $order->getGuaClientId(), $order->getStoreId());
            }
        } catch (Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('Error :' . $e->getMessage());
        }
    }

    protected function _setGuaOnSuccessPageView($orderIds) {
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_init');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
        $block = Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('google_universal_analytics_gtm');
        if ($block) {
            $block->setOrderIds($orderIds);
        }
    }
    
    protected function _saveGuaDataToQuote($quote, $saveNeeded = false) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId(false);
        if ($clientId && $quote->getGuaClientId() !== $clientId ) {
            $quote->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $quote->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            } else {
                $quote->setGuaUa(Mage::helper('core/http')->getHttpUserAgent());
            }
        }
        if($saveNeeded) {
            $quote->save();
        }
    }

    

    
    
    public function observeSalesQuotePaymentSaveAfter($observer) {
        $payment = $observer->getEvent()->getPayment();
        if($payment->getData('method')) {
            $this->_sendCheckoutPaymentMethod($payment);
        }
    }
    
    public function observeSalesQuoteAddressSaveAfter($observer) {
        $address = $observer->getEvent()->getQuoteAddress();
        if($address->getAddressType() == 'shipping' && $address->getData('shipping_method')) {
            $this->_sendCheckoutShippingMethod($address);
        }
        if($address->getData('city')) {
            $this->_sendCheckoutAddress($address);
        }
    }
    
    protected function _canSendCheckoutStep($stepIndex, $storeId) {
        if(!$stepIndex) {
            return false;
        }
        if($stepIndex > 0 && Mage::getSingleton('checkout/session')->getGuaCheckoutStep() != ($stepIndex-1)) {
            return false;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_checkout', $storeId)) {
            return false ;
        }
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable($storeId)) {
            return false;
        }
        return true;
    }
    
    protected function _sendCheckoutPaymentMethod($payment) {
        $storeId = $payment->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_payment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $payment->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->getMeasurementProtocol()->getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $payment->getMethod();
        $this->_sendEvent('checkout', 'payment', $payment->getMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutAddress($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = $quoteShippingAddress->getAddressType()=='billing' ? 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_billing_address', $storeId) : 
                Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipping_address', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
       
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->getMeasurementProtocol()->getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getCity() . ' ' . $quoteShippingAddress->getCountryId(); // We cannot add postcode here, see https://support.google.com/analytics/answer/6366371?hl=en#geolocation : " In some instances, such as in the UK, zip code can map to a single residence and thus cannot be passed to Analytics."
        $this->_sendEvent('checkout', 'address', $quoteShippingAddress->getAddressType(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutShippingMethod($quoteShippingAddress) {
        $storeId = $quoteShippingAddress->getQuote()->getStoreId();
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_shipment_method', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quoteShippingAddress->getQuote()->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->getMeasurementProtocol()->getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = $quoteShippingAddress->getShippingDescription()?$quoteShippingAddress->getShippingDescription():$quoteShippingAddress->getShippingMethod();
        $this->_sendEvent('checkout', 'shipping_method', $quoteShippingAddress->getShippingMethod(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutLogin($customer) {
        $storeId = Mage::helper('wcooall')->getCurrentStoreId(); // customer login is always on frontend.
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_login', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Group '.$customer->getGroupId();
        $this->_sendEvent('checkout', 'login', 'Customer ' . $customer->getId(), 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCart($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_cart', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->getMeasurementProtocol()->getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Cart';
        $this->_sendEvent('checkout', 'cart', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }
    
    protected function _sendCheckoutCheckout($cart) {
        if(!$cart || !is_object($cart) || !$cart->getQuote()) {
            return;
        }
        $quote = $cart->getQuote();
        $storeId = $quote->getStoreId(); 
        $stepIndex = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/checkout_step_checkout', $storeId);
        
        if(!$this->_canSendCheckoutStep($stepIndex, $storeId)) {
            return;
        }
        $params = array();
        $items = $quote->getAllVisibleItems();
        $itemIncrement = 1;
        foreach($items as $item) {
            $itemParams = $this->getMeasurementProtocol()->getItemParams($item, $itemIncrement++);
            $params = array_merge($params, $itemParams);
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'checkout';
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP] = $stepIndex;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CHECKOUT_STEP_OPTION] = 'Checkout';
        $this->_sendEvent('checkout', 'onepage', 'view', 1, false, false, $storeId, $params);
        Mage::getSingleton('checkout/session')->setGuaCheckoutStep($stepIndex);
    }

    protected function _sendRefundRequestFromCreditmemo($creditmemo, $order) {
        $storeId = $order->getStoreId();
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!$order->getGuaClientId()) {
            return;
        }
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable($storeId)) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $currency = $useStoreCurrency ? $creditmemo->getOrderCurrencyCode() : $creditmemo->getBaseCurrencyCode();

        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order);
        $items = $creditmemo->getAllItems();
        $refundTotal = $creditmemo->getGrandTotal();
        
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem && $item->getQty() > 0) {
                $itemPrice = $this->getEcHelper()->getItemPriceForCreditmemo($item);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQty();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
                $itemIncrement++;
            }
        }
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_REVENUE] = $refundTotal;
        //$params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY] = $currency;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $this->_sendEvent('transaction', 'creditmemo', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
        
    }

    
    

    

    

    protected function _refundCanceledOrder($order) {
        $storeId = $order->getStoreId();
      
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/manage_refund', $storeId)) {
            return;
        }
        if (!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/refund_canceled_order', $storeId)) {
            return;
        }
      
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $currency = $useStoreCurrency ? $order->getStoreCurrencyCode() : $order->getBaseCurrencyCode();
        
        $params = array();
        $guaClientId = $order->getGuaClientId();
        $transactionId = Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order);
        $items = $order->getAllItems();
        $refundTotal = $order->getBaseTotalCanceled();
        
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID] = $transactionId;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_REVENUE] = $refundTotal;
        //$params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY] = $currency;
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION] = 'refund';
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem && $item->getQtyCanceled() > 0) {
                $itemPrice = $this->getEcHelper()->getItemPriceForOrder($item);
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $item->getSku();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $item->getQtyCanceled();
                $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
                $itemIncrement++;
            }
        }
        
        $this->_sendEvent('sales', 'order_canceled', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);
       
    }


    protected function _sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        return $this->getMeasurementProtocol()
                ->sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $additionalParameters);
    }
    
}
