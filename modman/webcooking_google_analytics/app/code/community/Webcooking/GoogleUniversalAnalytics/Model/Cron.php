<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Cron {

    protected function _getApi() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
    
    protected $_productFeed = null;
    protected function _getProductFeed($storeId = false) {
        if(is_null($this->_productFeed)) {
            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $productCollection->addAttributeToSelect('name', 'left');
            $productCollection->addAttributeToSelect('price', 'left');
            $productCollection->addCategoryIds();
            //$productCollection->addAttributeToSelect('category_ids', 'left');
            $brandAttributeCode = Mage::helper('googleuniversalanalytics/ecommerce')->getBrandAttributeCode($storeId);
            if($brandAttributeCode) {
                $productCollection->addAttributeToSelect($brandAttributeCode, 'left');
            }
            $header = array('ga:productSku','ga:productBrand','ga:productCategoryHierarchy','ga:productName','ga:productPrice');
            $this->_productFeed = implode(',', $header) . "\n";
            Mage::getSingleton('core/resource_iterator')
                        ->walk($productCollection->getSelect(), array(array($this, '_addProductInFeed')), array('brandAttributeCode'=>$brandAttributeCode));
            $this->_productFeed = trim($this->_productFeed);
        }
        return $this->_productFeed;
        
    }
    
    public function _addProductInFeed($args) {
        $productData = $args['row'];
        $product = Mage::getModel('catalog/product');
        $product->setData($productData);
        $brandAttributeCode = $args['brandAttributeCode'];
        $row = array();
        $row[] = $productData['sku'];
        if($brandAttributeCode) {
            $row[] = '"' . Mage::helper('wcooall/attribute')->getAttributeOptionNameById($brandAttributeCode, $productData[$brandAttributeCode]) . '"';
        } else {
            $row[] = '';
        }
        $row[] = '"' . Mage::helper('googleuniversalanalytics/ecommerce')->getProductCategoryValue($product, false) . '"';
        $row[] = '"' . $productData['name'] . '"';
        $row[] = '"' . $productData['price'] . '"';
        $this->_productFeed .= implode(',' , str_replace($row, ",", "\,")) . "\n";
    }

    
    public function updateProducts() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/export_products_via_api')) {
            return;
        }
        $customDataSourceId = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/export_products_data_source_id');
        if(!$customDataSourceId) {
            return;
        }
        if(!$this->_getApi() || !$this->_getApi()->getClient()) {
            return;
        }
        try {
            $profile = $this->_getApi()->getProfileForStore();
            if($profile) {
                $productFeed = $this->_getProductFeed();
              
                $analytics = new Google_Service_Analytics($this->_getApi()->getClient());
                $result = $analytics->management_uploads->uploadData(
                        $profile->getAccountId(), 
                        Mage::helper('googleuniversalanalytics')->getAccountId(), 
                        $customDataSourceId, 
                        array('data' => $productFeed,
                              'mimeType' => 'application/octet-stream',
                              'uploadType' => 'media'));
            }
        } catch (apiServiceException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an Analytics API service error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch (apiException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was a general API error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch(Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an API error ' . $e->getMessage());
        }
        
    }
    
    
    public function sendQueuedHits() {
        $mp =  Mage::getModel('googleuniversalanalytics/measurement_protocol');
        $queuedHitCollection = Mage::getModel('googleuniversalanalytics/measurement_protocol_queue')->getCollection();
        foreach($queuedHitCollection as $queuedHit) {
            $mp->sendHitFromQueue($queuedHit);
            $queuedHit->delete();
            sleep(1);
        }
    }

}
