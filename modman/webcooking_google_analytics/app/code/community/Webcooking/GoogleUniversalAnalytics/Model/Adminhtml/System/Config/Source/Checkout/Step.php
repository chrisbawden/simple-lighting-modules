<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Checkout_Step extends Varien_Object {

    public function toOptionArray() {

        $attributes = array();
        $attributes[] = array('value' => '', 'label' => Mage::helper('googleuniversalanalytics')->__('Not in checkout tracking'));
        foreach (range(1,10) as $idx) {
            $attributes[] = array('value' => $idx, 'label' => $idx);
        }

        return $attributes;
    }

}
