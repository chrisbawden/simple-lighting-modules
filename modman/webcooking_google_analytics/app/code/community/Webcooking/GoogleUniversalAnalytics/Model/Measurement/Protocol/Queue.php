<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol_Queue extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('googleuniversalanalytics/measurement_protocol_queue');
    }

    protected function _beforeSave() {
        parent::_beforeSave();
        $date = Mage::getSingleton('core/date')->date('Y-m-d H:i:s');
        if(!$this->getId()) {
            $this->setCreatedAt($date);
        }
        if(is_array($this->getHitData())) {
            $this->setHitData(serialize($this->getHitData()));
        }
        return $this;
    }
    
}