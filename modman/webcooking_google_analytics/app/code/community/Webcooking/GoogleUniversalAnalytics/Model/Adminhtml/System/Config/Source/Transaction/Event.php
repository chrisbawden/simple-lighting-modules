<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Transaction_Event extends Varien_Object {
    
  public function toOptionArray() {
        $attributes = array();
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_ORDER, 'label' => Mage::helper('googleuniversalanalytics')->__('Order is created'));
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER, 'label' => Mage::helper('googleuniversalanalytics')->__('Order is placed'));
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CHANGE_ORDER_STATUS, 'label' => Mage::helper('googleuniversalanalytics')->__('Order status changed'));
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CREATE_INVOICE, 'label' => Mage::helper('googleuniversalanalytics')->__('Invoice is created'));
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE, 'label' => Mage::helper('googleuniversalanalytics')->__('Invoice is paid (payment pay event)'));
        $attributes[] = array('value' => Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PAY_INVOICE, 'label' => Mage::helper('googleuniversalanalytics')->__('Invoice is paid (invoice pay event)'));
        
        return $attributes;
    }
    
}
