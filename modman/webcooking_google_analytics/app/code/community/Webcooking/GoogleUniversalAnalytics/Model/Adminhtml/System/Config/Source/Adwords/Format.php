<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Adwords_Format extends Varien_Object {

   
    
    public function toOptionArray() {
        return array(
            '1' => Mage::helper('googleuniversalanalytics')->__('1-line notification to visitors'),
            '2' => Mage::helper('googleuniversalanalytics')->__('2-line notification to visitors'),
            '3' => Mage::helper('googleuniversalanalytics')->__('no notification to visitors'),
        );
    }

}
