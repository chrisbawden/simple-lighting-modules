<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Dimension extends Varien_Object {

    protected static $_attributes = null;
    
    public function toOptionArray() {
        if(is_null(self::$_attributes)) {
            self::$_attributes = array();
            self::$_attributes[] = array('value' => '', 'label' => Mage::helper('googleuniversalanalytics')->__('No dimension'));
            for ($idx= 1; $idx<=200; $idx++) {
                self::$_attributes[] = array('value' => $idx, 'label' => $idx);
            }
        }

        return self::$_attributes;
    }

}
