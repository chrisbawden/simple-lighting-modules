<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Adminhtml_Chart extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('webcooking/googleuniversalanalytics/chart.phtml');
        $this->setQuery('visitors');
    }
    
    public function setPeriod($period) {
        $oneDay = 24*60*60;
        switch($period) {
            case 'today':
                $this->setFrom(date('Y-m-d'));
                $this->setTo(date('Y-m-d'));
                break;
            case 'yesterday':
                $this->setFrom(date('Y-m-d', time()-$oneDay));
                $this->setTo(date('Y-m-d', time()-$oneDay));
                break;
            case 'thisweek':
                $weekDay = date("w");
                $firstDayOfWeek = Mage::getStoreConfig('general/locale/firstday');
                if($firstDayOfWeek != 0 && $firstDayOfWeek < $weekDay) {
                    $weekDay += 7;
                }
                $diff = $firstDayOfWeek - $weekDay;
                $this->setFrom(date('Y-m-d', time() + $oneDay*$diff));
                $this->setTo(date('Y-m-d', time()));
                break;
            case 'thismonth':
                $this->setFrom(date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y'))));
                $this->setTo(date('Y-m-d', time()));
                break;
            case 'thisyear':
                $this->setFrom(date('Y-m-d', mktime(0, 0, 0, 1, 1, date('Y'))));
                $this->setTo(date('Y-m-d', time()));
                break;
            case (preg_match("%^last([0-9]+)days$%i", $period, $pregResult) ? $period : !$period):
                $days = $pregResult[1];
                $this->setFrom(date('Y-m-d', time()-$oneDay*$days));
                $this->setTo(date('Y-m-d'));
                break;
        }
    }
    
    
    public function getChartJson() {
        $data = $this->getChartData();
        return $data['json'];
    }
    
    
    public function getChartTotal() {
        $data = $this->getChartData();
        return $data['totalResults'];
    }
    
    public function getChartTotalForAllResults() {
        $data = $this->getChartData();
        return $data['totalsForAllResults']['ga:'.$this->getQuery()];
    }
    
    public function getChartData() {
        return Mage::helper('googleuniversalanalytics/dashboard')->getChartData($this->getFrom(), $this->getTo(), $this->getQuery());
    }
    
    public function getHAxisTitle() {
        if($this->getData('h_axis_title')) {
            return $this->getData('h_axis_title');
        }
        if($this->getFrom() == $this->getTo()) {
            return $this->__('Hours');
        }
        return $this->__('Dates');
    }
    
    public function getVAxisTitle() {
        if($this->getData('v_axis_title')) {
            return $this->getData('v_axis_title');
        }
        return ucwords($this->getQuery());
    }
}
