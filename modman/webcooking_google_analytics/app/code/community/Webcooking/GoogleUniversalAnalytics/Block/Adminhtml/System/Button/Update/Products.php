<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Adminhtml_System_Button_Update_Products extends Webcooking_All_Block_Adminhtml_System_Config_Button
{
    

    public function getButtonUrl()
    {
        return Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/gua_api/updateproducts');
    }

    public function getButtonLabel()
    {
        return $this->helper('googleuniversalanalytics')->__('Update products');
    }

    
    public function getIsAjaxButton()
    {
        return true;
    }

}
