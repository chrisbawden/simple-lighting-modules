<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */

class Webcooking_GoogleUniversalAnalytics_GuaController extends Mage_Core_Controller_Front_Action
{
    
    protected function _isValidSession() {
        if(preg_match('%(Googlebot|bingbot|msnbot)%i', Mage::helper('core/http')->getHttpUserAgent())) {
            return false;
        }
        return true;
    }
    
    /**
     * @deprecated
     */
   public function updatesessionAction() {
       if(!$this->_isValidSession()) {
           return;
       }
       $guaClientId = $this->getRequest()->getParam('clientid');
       $session = Mage::getSingleton('core/session');
       $session->setGuaClientId($guaClientId);
       $session->setGuaUserAgent(Mage::helper('core/http')->getHttpUserAgent());
   }
   
   public function testAction() {
       /*
       $test = array(
       'v' => 1,
        'ds' => 'web',
        'uip' => '90.16.254.110',
        'tid' => '',
        'ua' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36',
        'dh' => 'dev.twonav.com/fr/',
        'cid' => '.',
        't' => 'event',
        'ec' => 'transaction',
        'ea' => 'invoice',
        'el' => 'OR-2017-6-017020',
        'ev' => 299,
        'ni' => 1,
        'uid' => 3,
        'pr1id' => 'twonav_anima_generic_fr-twonav_anima_escandallo_dispositivo-1205',
        'pr1nm' => 'Anima',
        'pr1ca' => 'GPS',
        'pr1br' => '',
        'pr1va' => '',
        'pr1pr' => 299.0000,
        'pr1qt' => 1,
        'ti' => 'OR-2017-6-017020',
        'ta' => 'FranÃ§ais',
        'tr' => 299,
        'ts' => 0,
        'tt' => 51.8926,
        'pa' => 'purchase',
        'cu' => 'EUR'
           );
       echo "<pre>";
       var_dump($test);
       echo "</pre>";
       $result = Mage::getModel('googleuniversalanalytics/measurement_protocol')->execRequest($test);
       var_dump($result);*/
   }
}