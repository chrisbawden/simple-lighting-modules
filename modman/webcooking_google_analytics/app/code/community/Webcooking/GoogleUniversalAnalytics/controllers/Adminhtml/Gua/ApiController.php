<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Adminhtml_Gua_ApiController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_GoogleUniversalAnalytics');
    }

    public function updateproductsAction() {
        $cronModel = Mage::getModel('googleuniversalanalytics/cron');
        $cronModel->updateProducts();
    }

    
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin');
    }
    

}
