<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('googleuniversalanalytics/queue'))
    ->addColumn('queue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Queue Id')
    ->addColumn('hit_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        ), 'Hit Data')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Created At')
    ->setComment('GUA Measurement Protocol Hit Queue');
$installer->getConnection()->createTable($table);

$installer->endSetup(); 
 