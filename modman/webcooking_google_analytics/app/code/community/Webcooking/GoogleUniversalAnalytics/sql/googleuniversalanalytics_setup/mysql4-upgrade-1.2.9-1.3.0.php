<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();
try {
    $installer->run("
    ALTER TABLE  `" . $this->getTable('sales/order') . "` ADD  `gua_ua` VARCHAR(512) NULL;
    ");
} catch(Exception $e) {
    if(!preg_match('%Column already exists%', $e->getMessage())) {
        throw $e;
    }
}
$installer->endSetup(); 
