<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Optimize extends Webcooking_All_Helper_Data {
 
    public function isActive($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/optimize/active', $store) && $this->getContainerId($store);
    }
    
    public function shouldAddPageHidingSnippet($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/optimize/active_page_hiding_snippet', $store) && $this->isActive($store);
    }
    
     public function getContainerId($store = null) {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive())  {
            return Mage::helper('googleuniversalanalytics/gtm')->getAccountId();
        }
        return Mage::getStoreConfig('googleuniversalanalytics/optimize/container_id', $store);
    }
    
    
}