<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Data extends Webcooking_All_Helper_Data {

    const TRANSACTION_EVENT_CREATE_ORDER = 'create_order';
    const TRANSACTION_EVENT_PLACE_ORDER = 'place_order';
    const TRANSACTION_EVENT_CHANGE_ORDER_STATUS = 'change_order_status';
    const TRANSACTION_EVENT_CREATE_INVOICE = 'create_invoice';
    const TRANSACTION_EVENT_CAPTURE_INVOICE = 'capture_invoice';
    const TRANSACTION_EVENT_PAY_INVOICE = 'pay_invoice';
    
    
    protected $_customDimensionConfigPath = 'googleuniversalanalytics/dimensions/';

    public function generateUuid() {
        $cookieName = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_name');
        $cookieName = $cookieName ? $cookieName : '_ga';
        $cookieValue = Mage::getModel('core/cookie')->get($cookieName);
        if($cookieValue) {
            return preg_replace('%GA1\.[0-9]+\.%', '', $cookieValue);
        }
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    /*
    public function generateUuid() {
        $md5 = md5(uniqid('', true));
        return substr($md5, 0, 8) . '-' .
                substr($md5, 8, 4) . '-' .
                substr($md5, 12, 4) . '-' .
                substr($md5, 16, 4) . '-' .
                substr($md5, 20, 12);
    }*/

    public function getClientId($createIfNotFound = false) {
        try {
            $coreSession = Mage::getSingleton('core/session');
        } catch(Exception $e) {
            return;
        }
        $guaClientId = $this->generateUuid();
        if((!$coreSession->getGuaClientId() && $createIfNotFound) ||
            $coreSession->getGuaClientId() !== $guaClientId) {
            $coreSession->setGuaClientId($guaClientId);
            if($coreSession->getGuaClientId()) { // If somehow $coreSession->getGuaClientId() return empty, there is risks of infinite loop when sending the event
                if(!Mage::registry('GUA_NEW_CLIENT_ID_NEED_SESSION_START')) {
                    Mage::register('GUA_NEW_CLIENT_ID_NEED_SESSION_START', true);
                }
            }
        }
        return $coreSession->getGuaClientId();
    }

    public function getAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/account', $store);
    }

    public function isActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/active', $store);
    }

    public function isGoogleAnalyticsAvailable($store = null) {
        return $this->getAccountId($store) && $this->isActive($store);
    }

    
    public function getGlobalAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/global_tracker_account', $store);
    }

    public function isGlobalActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/global_tracker_active', $store) && $this->getGlobalAccountId($store);
    }

    public function isGoogleAnalyticsGlobalAccountAvailable($store = null) {
        return $this->getGlobalAccountId($store) && $this->isGlobalActive($store) && Mage::getStoreConfig('googleuniversalanalytics/general/global_tracker_account', $store) != Mage::getStoreConfig('googleuniversalanalytics/general/account', $store);
    }

    
    public function isDemographicsActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_demographics', $store);
    }
    
    public function isIPAnonymized($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/anonymize_ip', $store);
    }
    
    
    
    
    public function isOptOutActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_opt_out', $store);
    }
    
   
    
    
    public function getGaCreateOptions($store = null, $asJSON = true) {
        $cookieName = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_name');
        $cookieDomain = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_domain');
        $cookieExpiration = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_expiration');
        $allowLinker = Mage::getStoreConfigFlag('googleuniversalanalytics/options/allow_linker');
        $linkerAcceptIncoming = Mage::getStoreConfigFlag('googleuniversalanalytics/options/linker_accept_incoming');
        $linkerDomains = explode("\n", Mage::getStoreConfigFlag('googleuniversalanalytics/options/linker_domains'));
        $linkerDomains = array_filter($linkerDomains);
        $allowDisplayFeatures = Mage::getStoreConfigFlag('googleuniversalanalytics/options/allow_display_features');
        $enableLinkAttribution = Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_link_attribution');
        $anonymizeIp = $this->isIPAnonymized($store);
        $optimizeId = Mage::helper('googleuniversalanalytics/optimize')->isActive() ? Mage::helper('googleuniversalanalytics/optimize')->getContainerId() : false;
        $userId = false;
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_user_id') && Mage::getSingleton('customer/session')->isLoggedIn()) {
            $userId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }
        $options = array();
        if(!$allowDisplayFeatures) { // default = true
            $options['allow_display_features'] = $allowDisplayFeatures;
        }
        if($cookieName) {
            $options['cookie_name'] = $cookieName;
        }
        if($cookieDomain) {
            $options['cookie_domain'] = $cookieDomain;
        }
        if($cookieExpiration) {
            $options['cookie_expires'] = $cookieExpiration;
        }
        
        if($anonymizeIp) { // default = false
            $options['anonymize_ip'] = $anonymizeIp;
        }
        
        if($enableLinkAttribution) {
            $options['link_attribution'] = $enableLinkAttribution;
        }
        
        
        
        
        if($allowLinker && ($linkerAcceptIncoming || $linkerDomains)) {
            $options['linker'] = array();
            if($linkerAcceptIncoming) {
                $options['linker']['accept_incoming'] = true;
            }
            if($linkerDomains) {
                $options['linker']['domains'] = $linkerDomains;
            }
        }
        
        /* User ID */
        if($userId) {
            $options['user_id'] = $userId;
        }
        
        /* Optimize */
        if($optimizeId) {
            $options['optimize_id'] = $optimizeId;
        }
        
        
        /* Custom dimensions */
        $customDimensions = $this->getCustomDimensions();
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $options['dimension'.$customDimensionIndex] = $customDimensionValue;
        }
        
        
        if($asJSON) {
            if(empty($options)) {
                return false;
            }
            return Zend_Json::encode($options);
        }
        return $options;
    }
    
    public function getRemarketingConversionId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/remarketing/conversion_id', $store);
    }
    
    public function useRemarketing($store = null) {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/remarketing/active', $store)) {
            return false;
        }
        if(!$this->getRemarketingConversionId($store)) {
            return false;
        }
        return true;
    }
    
    public function getAdwordsConversionId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/conversion_id', $store);
    }
    
    public function getAdwordsConversionLabel($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/label', $store);
    }
    
    public function getAdwordsConversionFormat($store = null) {
        return min(3, max(1, intval(Mage::getStoreConfig('googleuniversalanalytics/adwords/format', $store))));
    }
    
    public function getAdwordsConversionValue($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/value', $store);
    }
    
    public function isAdwordsConversionValueInclTax($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/adwords/grand_total_incl_tax', $store);
    }
    
    public function isAdwordsConversionValueInclShipping($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/adwords/grand_total_incl_shipping', $store);
    }
    
    public function useAdwords($store = null) {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/adwords/active', $store)) {
            return false;
        }
        if(!$this->getAdwordsConversionId($store)) {
            return false;
        }
        return true;
    }
    
    public function getPageId() {
        $request = Mage::app()->getRequest();
        if ($request->getModuleName() == 'catalog' && $request->getControllerName() == 'product' && $request->getActionName() == 'view') {
            $_product = Mage::registry('current_product');
            if ($_product->getId()) {
                return 'product'.$_product->getId();
            } else {
                return 'product0';
            }
        }

        if ($request->getModuleName() == 'catalog' && $request->getControllerName() == 'category' && $request->getActionName() == 'view') {
            $_category = Mage::registry('current_category');
            if ($_category->getId()) {
                return 'category'.$_category->getId();
            } else {
                return 'category0';
            }
        }

        if ($request->getModuleName() == 'checkout' && $request->getControllerName() == 'cart' && $request->getActionName() == 'index') {
            return 'cart';
        }

        if ($request->getModuleName() == 'cms' && $request->getControllerName() == 'index' && $request->getActionName() == 'index') {
            $_page = Mage::registry('current_page');
            if($_page && $_page->getId()) {
                return 'cms'.$_page->getId();
            } else {
                return 'cms0';
            }
        }
        
        $action = Mage::app()->getFrontController()->getAction();
        return $action?$action->getFullActionName():'';
    }
    
    
    
    
    public function getCustomDimensionsForProduct($product) {
        $customDimensions = array();
        if(!$product || !$product->getId()) {
            return array();
        }
        
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_id'))) {
            $customDimensions[$idx] = $product->getId();
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_sku'))) {
            $customDimensions[$idx] = $product->getSku();
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_name'))) {
            $customDimensions[$idx] = $this->formatData($product->getName());
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_price'))) {
            $customDimensions[$idx] = Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), false);
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_cost'))) {
            $customDimensions[$idx] = $product->getCost();
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'product_profit'))) {
            $customDimensions[$idx] = $product->getFinalPrice() - $product->getCost();
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'unirgy_dropship_vendor')) && Mage::getModel('udropship/vendor')) {
            $_vendor = Mage::getModel('udropship/vendor')->load(Mage::helper('udropship')->getProductVendorId($product));
            $customDimensions[$idx] = $_vendor->getName();
        }
        return $customDimensions;
    }
    
    public function getCustomDimensions($customer = null, $product = null, $category = null, $page = null, $quote = null) {
        $customDimensions = array();
        if(is_null($customer)) {
            $customer = Mage::helper('customer')->getCustomer();
            if(!$customer->getId()) {
                $customer = false;
            }
        }
        if(is_null($product)) {
            $product = Mage::registry('current_product');
        }
        if(is_null($category)) {
            $category = Mage::registry('current_category');
        }
        if(is_null($page)) {
            $page = Mage::registry('current_page');//todo with observer on cms_page_render
        }
        if(is_null($quote) && !Mage::getSingleton('core/session')->getGuaCustomerLogout()) {
            if(!Mage::registry('prevent_observe_salesrule_validator_process')) Mage::register('prevent_observe_salesrule_validator_process', 1);
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            if(!$quote->getId() || $quote->getItemsQty() <= 0) {
                $quote = false;
            }
        }
        $action = Mage::app()->getFrontController()->getAction();
        $layout = Mage::app()->getLayout();
        
        /* ENV */
        if($action && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'env_page'))) {
            $customDimensions[$idx] = $action->getFullActionName();
        }
        if(($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'env_demo'))) {
            $customDimensions[$idx] = Mage::getStoreConfigFlag('design/head/demonotice');
        }
        if(($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'env_client_id'))) {
            $customDimensions[$idx] = $this->getClientId(false);
        }
        if(($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'timestamp'))) {
            //$timestamp = date('c', Mage::getModel('core/date')->timestamp(time()));
            $date = new Zend_Date(microtime(true));
            $date->setFractionalPrecision(2);
            list($milli, $time) = explode(" ", microtime());
            $date->setMilliSecond(intval($milli*100));
            $timestamp = $date->toString('yyyy-MM-ddThh:mm:ss.SZZZZ');
            $customDimensions[$idx] = $timestamp;
        }
        if(($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'session_id'))) {
            if(!Mage::getSingleton('core/session')->getGuaCdSessionId()) {
                Mage::getSingleton('core/session')->setGuaCdSessionId(uniqid('gua', true));
            }
            $customDimensions[$idx] = Mage::getSingleton('core/session')->getGuaCdSessionId();
        }
        if(($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'g_session_id'))) {
            //$timestamp = date('c', Mage::getModel('core/date')->timestamp(time()));
            $date = new Zend_Date(microtime(true));
            $date->setFractionalPrecision(2);
            list($milli, $time) = explode(" ", microtime());
            $date->setMilliSecond(intval($milli*100));
            $timestamp = $date->toString('yyyy-MM-ddThh:mm:ss.SZZZZ');
            $customDimensions[$idx] = $timestamp;
        }
        
        /* Breadcrumb info */
        $cpt = 1;
        foreach(Mage::helper('catalog')->getBreadcrumbPath() as $crumbId => $crumbInfo) {
            if($cpt > 5) {
                return;
            }
            if($crumbId == 'product' && $product && $product->getId()) {
                $crumbId .= $product->getId();
            }
            if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'breadcrumb_id'.$cpt))) {
                $customDimensions[$idx] = $crumbId;
            }
            if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'breadcrumb_label'.$cpt))) {
                $customDimensions[$idx] = $crumbInfo['label'];
            }
            $cpt++;
        }
        
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'page_name'))) {
            $customDimensions[$idx] = $product?$product->getName():($category?$category->getName():($page?$page->getTitle():''));
        }
        
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'page_id'))) {
            $customDimensions[$idx] = $this->getPageId();
        }
        
        
        /* CUSTOMER */
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_logged_in'))) {
            $customDimensions[$idx] = Mage::getSingleton('customer/session')->isLoggedIn()?'1':'0';
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_id'))) {
            $customDimensions[$idx] = $customer->getId();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_group_id'))) {
            $customDimensions[$idx] = $customer->getGroupId();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_group_name'))) {
            $customDimensions[$idx] = Mage::getSingleton('customer/group')->load($customer->getGroupId())->getData('customer_group_code');
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_orders_count'))) {
            if(!Mage::getSingleton('core/session')->getGuaCustomerOrderCount()) {
                $orderCollection = Mage::getModel('sales/order')->getCollection();
                $orderCollection->addFieldToFilter('customer_id',$customer->getId());   
                $orderCollection->addFieldToFilter('state',Mage_Sales_Model_Order::STATE_COMPLETE);   
                Mage::getSingleton('core/session')->setGuaCustomerOrderCount($orderCollection->count());
            }
            $customDimensions[$idx] = Mage::getSingleton('core/session')->getGuaCustomerOrderCount();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_last_order_date'))) {
            if(!Mage::getSingleton('core/session')->getGuaCustomerLastOrderDate()) {
                $orderCollection = Mage::getModel('sales/order')->getCollection();
                $orderCollection->addFieldToFilter('customer_id',$customer->getId());   
                $orderCollection->addFieldToFilter('state',Mage_Sales_Model_Order::STATE_COMPLETE);
                $orderCollection->setOrder('created_at', 'DESC');
                $orderCollection->setPageSize(1);
                if($orderCollection->getSize()) {
                    Mage::getSingleton('core/session')->setGuaCustomerLastOrderDate($orderCollection->getFirstItem()->getCreatedAt());
                }
            }
            $customDimensions[$idx] = Mage::getSingleton('core/session')->getGuaCustomerLastOrderDate();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_spent_amount'))) {
            if(!Mage::getSingleton('core/session')->getGuaCustomerSpentAmount()) {
                $orderCollection = Mage::getModel('sales/order')->getCollection();
                $orderCollection->addFieldToFilter('customer_id',$customer->getId());   
                $orderCollection->addFieldToFilter('state',Mage_Sales_Model_Order::STATE_COMPLETE);
                $orderCollection->getSelect()->reset(Zend_Db_Select::COLUMNS);
                $orderCollection->getSelect()
                        ->columns('SUM(base_grand_total) as customer_spent_amount');
                Mage::getSingleton('core/session')->setGuaCustomerSpentAmount($orderCollection->getFirstItem()->getCustomerSpentAmount());
               
            }
            $customDimensions[$idx] = Mage::getSingleton('core/session')->getGuaCustomerSpentAmount();
        }
        
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_email'))) {
            $customDimensions[$idx] = $customer->getEmail();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_name'))) {
            $customDimensions[$idx] = $customer->getName();
        }
        if ($customer && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_gender'))) {
            $customDimensions[$idx] = $customer->getResource()->getAttribute('gender')->getSource()->getOptionText($customer->getGender());
        }
        if ($customer && $customer->getPrimaryBillingAddress() && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'customer_postcode'))) {
            $customDimensions[$idx] = $customer->getPrimaryBillingAddress()->getPostcode();
        }
        
        
        /* CART */
        if ($quote && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'quote_id'))) {
            $customDimensions[$idx] = $quote->getId();
        }
        if ($quote && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'quote_grand_total'))) {
            $customDimensions[$idx] = $quote->getGrandTotal();
        }
        if ($quote && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'quote_items_count'))) {
            $customDimensions[$idx] = $quote->getItemsCount();
        }
        if ($quote && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'quote_items_qty'))) {
            $customDimensions[$idx] = $quote->getItemsQty();
        }
        if ($quote && ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'quote_coupon_code'))) {
            $customDimensions[$idx] = $quote->getCouponCode();
        }
        
        
        /* REMARKETING */
        if ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'prod_id')) {
            $customDimensions[$idx] = str_replace("'", "", Mage::helper('googleuniversalanalytics/remarketing')->getProdId());
        }
        if ($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'page_type')) {
            $customDimensions[$idx] = Mage::helper('googleuniversalanalytics/remarketing')->getPageType();
        }
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'total_value')) && Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue()) {
            $customDimensions[$idx] = Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue();
        }
        
        
        /* Extensions */
        $session = Mage::getSingleton('checkout/session');
        if (($idx = Mage::getStoreConfig($this->_customDimensionConfigPath . 'aw_points_used_points')) && $session->getData('use_points') && $session->getData('points_amount')) {
            $customDimensions[$idx] = $session->getData('points_amount');
        }
        
        /* PRODUCTS */
        $customDimensions = $customDimensions + $this->getCustomDimensionsForProduct($product);
        
        $eventTransportObject = new Varien_Object();
        $eventTransportObject->setCustomDimensions($customDimensions);
        Mage::dispatchEvent('googleuniversalanalytics_get_custom_dimensions', array('object' => $eventTransportObject));
        $customDimensions = $eventTransportObject->getCustomDimensions();
        
        foreach($customDimensions as $idx=>$value) {
            $customDimensions[$idx] = substr($value, 0, 150); // We don't use mb_substr since GA is limiting in Bytes, not in Characters..
            // GA performs a url encoding before sending the hit, resulting to an increase  of the string size (' ' becomes '%20' for instance, so 2 more bytes). And the limit is calculated on that url-encoded string...
            while(strlen(rawurlencode($customDimensions[$idx])) > 150) {
                $customDimensions[$idx] = substr($customDimensions[$idx], 0, -1);
            }
        }
        return $customDimensions;
    }
    
    
    public function log($message, $file="gua.log", $force=true) {
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/tools/log')) {
            Mage::log($message, null, $file, $force);
        }
    }
    
    public function formatData($data) {
       $data = strip_tags($data);
       $data = str_replace('"', '', $data);
       $data = str_replace("'", ' ', $data);
       return @iconv('UTF-8', 'UTF-8//IGNORE', $data);
   }
   
   public function shouldSendImpressionEventTag() {
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
           return false;
       }
       return Mage::getSingleton('core/session')->getShouldSendImpressionEventTag();
   }
    
   
   public function getTimeOnPageDelays() {
       $timeOnPage = explode(',', Mage::getStoreConfig('googleuniversalanalytics/events/time_on_page'));
        foreach($timeOnPage as $idx=>$delay) {
            $delay = intval($delay);
            if(!$delay) {
                unset($timeOnPage[$idx]);
            } else {
                $timeOnPage[$idx] = $delay;
            }
        }
        $timeOnPage = array_filter($timeOnPage);
        return $timeOnPage;
   }
   
   
   public function getReferralExclusionList($storeId = null) {
       $domains = explode("\n", Mage::getStoreConfig('googleuniversalanalytics/options/referral_exclusion_list', $storeId));
       foreach($domains as $idx=>$domain) {
           $domains[$idx] = preg_replace('%https?://%', '', $domains[$idx]);
           $domains[$idx] = str_replace("'", "", $domains[$idx]);
           $domains[$idx] = trim($domains[$idx]);
           $domains[$idx] = trim($domains[$idx], '/');
       }
       $domains = array_filter($domains);
       $domains = array_unique($domains);
       $domains = array_values($domains);
       return $domains;
   }
   
   
   public function getDocumentPath($path, $storeId=null) {
       $baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
       $parsedUrl = parse_url($baseUrl);
       if(isset($parsedUrl['path'])) {
           $path = preg_replace('%//+%', '/', $parsedUrl['path'].$path);
           $path = preg_replace('%(index.php/)+%', '$1', $path);
       }
       return $path;
   }
   
   public function getDocumentHostFromUrl($url = false, $storeId=null) {
       if(!$url) {
           $url = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
       }
       $parsedUrl = parse_url($url);
       return isset($parsedUrl['path'])?$parsedUrl['path']:'/';
   }
   public function getDocumentHost($url = false, $storeId=null) {
       if(!$url) {
           $url = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
       }
       $parsedUrl = parse_url($url);
       return isset($parsedUrl['host'])?$parsedUrl['host']:preg_replace('%^https?://%', '', $url);
   }
   
   
   public function saveGuaClientIdToOrder($order, $createIfNotFound=true) {
        $clientId = Mage::helper('googleuniversalanalytics')->getClientId($createIfNotFound);
        if ($clientId && (!$order->getGuaClientId() ||
                $order->getGuaClientId() !== $clientId)) {
            $order->setGuaClientId($clientId);
            if(Mage::getSingleton('core/session')->getGuaUserAgent()) {
                $order->setGuaUa(Mage::getSingleton('core/session')->getGuaUserAgent());
            } else {
                $order->setGuaUa(Mage::helper('core/http')->getHttpUserAgent());
            }
        }
    }
}