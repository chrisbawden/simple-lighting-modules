<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Gtm extends Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce {
 
    public function isActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/active', $store);
    }
    
    public function shouldAddContainer($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/add_tag', $store) && $this->getAccountId($store);
    }
    
     public function getAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/gtm/account', $store);
    }
    
    public function getAddProductDetailsTag($product, $withScriptTags = false) {
        $html = '';

        if ($withScriptTags) {
            $html = '<script>';
        }

        $html .= sprintf("
                        var productDetail = {
                             'id': '%s',
                             'name': '%s',
                             'category': '%s',
                             'brand': '%s',
                             'price': '%s',
                             'variant': '%s'
                         };
                         dataLayer.push({
                            'ecommerce': {
                                'currencyCode': '%s',
                                'detail': {
                                  'products': [productDetail]
                                 }
                                }
                          });
                         ", 
                $this->getProductSkuValue($product), 
                $this->getProductNameValue($product), 
                $this->getProductCategoryValue($product), 
                $this->getProductBrandValue($product), 
                $this->getProductPriceValue($product), 
                $this->getProductVariantValue($product),
                Mage::app()->getStore()->getCurrentCurrencyCode()
        );


        if ($withScriptTags) {
            $html .= '</script>';
        }
        
        return $html;
   }
   
   public function getTimeOnPageEventCode() {
        $guaCode = '';
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){   
                            dataLayer.push({
                                'event':'GAevent',
                                'eventCategory':'timeOnPage',
                                'eventAction':'{$delay} seconds',
                                'eventNoInteraction':true 
                            });       
                        }, {$delayMs});    " . "\n";
        }
        
        return $guaCode;
   }
           
   
   public function getCustomDimensionsCode() {
       $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        
        if(empty($customDimensions)) {
            return '';
        }

        $guaCode = 'dataLayer.push({' . "\n";

        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $customDimensionValue = str_replace("'", "\'", $customDimensionValue);
            $guaCode .= "'dimension{$customDimensionIndex}': '{$customDimensionValue}'," . "\n";
        }
        $guaCode .= '});';
        return $guaCode;
   }
   
   public function getOrdersTrackingCode($orderIds)
    {
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    { 
                    'id': '%s', 
                    'name': '%s', 
                    'sku': '%s', 
                    'category': '%s', 
                    'price': '%s', 
                    'quantity': '%s' 
                    }
                    ",
                    Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($item->getSku()), 
                    null,//TODO: should be tested : $this->jsQuoteEscape(Mage::helper('googleuniversalanalytics/ecommerce')->getProductCategoryValue($item->getProduct(), false, $order->getStoreId())), 
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item), 
                    round($item->getQtyOrdered())
                );
            }
            
            
               $result[] = sprintf("
                   dataLayer = [{ 
                        'transactionId': '%s',  
                        'transactionAffiliation': '%s', 
                        'transactionTotal': '%s',
                        'transactionShipping': '%s', 
                        'transactionTax': '%s', 
                        'transactionProducts': [%s] 
                      }]; 
                 ",
                Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                implode(',',$items)
            );
            
        }
        $guaCode = implode("\n", $result);
        
        return $guaCode;
    }
    
    
    public function getEnhancedOrdersTrackingCode($orderIds)
    {
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
       
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
        
            $items = array();
            foreach ($order->getAllVisibleItems() as $item) {
                $items[] = sprintf("
                    {
                    'id': '%s',
                    'name': '%s',
                    'category': '%s',
                    'brand': '%s',
                    'variant': '%s',
                    'price': '%s',
                    'quantity': '%s'
                   }
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                    dataLayer.push({
                        'value': %s,
                        'transactionId': %s,
                        'currency': %s,
                        'ecommerce': {
                          'purchase': {
                            'actionField': {
                              'id': '%s',                         
                              'affiliation': '%s',
                              'revenue': '%s',                
                              'tax':'%s',
                              'shipping': '%s',
                              'coupon': '%s'
                            },
                            'products': [%s]
                          }
                        }
                    });
                 ",
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order),
                $order->getOrderCurrencyCode(),
                Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                strtoupper($order->getCouponCode()),
                implode(',', $items)
            );


        }
        $guaCode =  implode("\n", $result);
        
        return $guaCode;
    }
    
    
    /**
     * 
     * @deprecated
     */
    public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true) {
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       $currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
        $html .= sprintf("
                     dataLayer.push({
                        'ecommerce': {
                          'currencyCode': '%s',                       
                          'impressions': [
                           {    
                             'id': '%s',
                             'name': '%s', 
                             'category': '%s',
                             'brand': '%s',
                             'variant': '%s',
                             'price': '%s',
                             'list': '%s',
                             'position': %s
                           }
                           ]
                        }
                      });
                     ",
                     $currency,
                     $skuValue,
                     $nameValue,
                     $categoryValue,
                     $brandValue,
                     $variantValue,
                     $priceValue,
                     $this->formatData($listName),
                     intval($position)
                 );
       


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       
       return $html;
   }
   
   
   /**
     * 
     * @deprecated
     */
   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true) {
     
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
      
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }

       $html .= sprintf("
                    dataLayer.push({
                        'ecommerce': {
                          'promoView' : {
                            'promotions': [
                             {    
                               'id': '%s',
                               'name': '%s', 
                               'creative': '%s',
                               'position': '%s',
                             }
                             ]
                           }
                        }
                      });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;

   }
    
}