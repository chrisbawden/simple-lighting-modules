<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
abstract class Webcooking_All_Controller_Router_Abstract extends Mage_Core_Controller_Varien_Router_Standard {

   abstract protected function _getModuleAlias($request);
   abstract protected function _getModuleName($request);
   
   abstract protected function _getControllerName($request);
   abstract protected function _getActionName($request);
   abstract protected function _getRouteName($request);
   protected function _actionBeforeDispath($request) {
       return $request;
   }

   protected function _getIdentifier($request) {
       return $this->_getRouteName($request);
   }
   
   protected function _getMaxLevels() {
       return 2;
   }

   public function match(Zend_Controller_Request_Http $request) {
        if (!$this->_beforeModuleMatch()) {
            return false;
        }
        if($request->isDispatched()) {
            return false;
        }

        
        $path = explode('/', trim($request->getPathInfo(), '/'));

        // If path doesn't match your module requirements
        if (count($path) > $this->_getMaxLevels() || $path[0] != $this->_getRouteName($request)) {
            return false;
        }
      
        $realModule = $this->_getModuleName($request);
        $controller = $this->_getControllerName($request);
        $action = $this->_getActionName($request);
        $controllerClassName = $this->_validateControllerClassName(
                        $realModule,
                        $controller
        );
        // If controller was not found
        if (!$controllerClassName) {
            return false;
        }
        // Instantiate controller class
        $controllerInstance = Mage::getControllerInstance(
                        $controllerClassName,
                        $request,
                        $this->getFront()->getResponse()
        );
        // If action is not found
        if (!$controllerInstance->hasAction($action)) {
            return false; // 
        }

        // Set request data
        $request->setRouteName($this->_getRouteName($request));
        $request->setModuleName($this->_getModuleAlias($request));
        $request->setControllerName($controller);
        $request->setActionName($action);
        $request->setControllerModule($realModule);
        $request = $this->_actionBeforeDispath($request);
        if(!$request) {
            return false;
        }
        
        $request->setAlias(
            Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
            $this->_getIdentifier($request)
        );
        $request->setDispatched(true);
       
        $controllerInstance->dispatch($action);
        // Indicate that our route was dispatched
        return true;
    }



}