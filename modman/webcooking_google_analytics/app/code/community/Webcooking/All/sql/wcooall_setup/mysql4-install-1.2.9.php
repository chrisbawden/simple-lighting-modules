<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;
$installer->startSetup();



$table = $installer->getConnection()
    ->newTable($installer->getTable('wcooall/task'))
    ->addColumn('task_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Task Id')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        ), 'Task status')
    ->addColumn('priority', Varien_Db_Ddl_Table::TYPE_INTEGER, null,  array(
        ), 'Task priority')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 256, array(
        ), 'Task name')
    ->addColumn('method', Varien_Db_Ddl_Table::TYPE_TEXT, 512, array(
        ), 'Task method')
    ->addColumn('params', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        ), 'Task params')
    ->addColumn('messages', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        ), 'Task messages')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Created At')
    ->addColumn('scheduled_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Scheduled At')
    ->addColumn('executed_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Executed At')
    ->addColumn('finished_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Finished At')
    ->setComment('WebCooking Cron Tasks');
$installer->getConnection()->createTable($table);


$installer->endSetup(); 
