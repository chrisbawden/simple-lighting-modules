<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Catalog_Product_List extends Mage_Catalog_Block_Product_List {

    protected $_limit = false;
    protected $_offset = false;
    protected $_toolbar = true;
    protected $_order = false;
    protected $_category = null;

    
    public function _construct() {
        parent::_construct();
        if(Mage::registry('category') && !$this->getCategoryId()) {
            $this->setCategoryId(Mage::registry('category')->getId());
        }
        if(Mage::registry('current_category') && !$this->getCategoryId()) {
            $this->setCategoryId(Mage::registry('current_category')->getId());
        }
    }
    
    
    /**
     *
     * @param <type> $attributeName
     * @param <type> $operator One or several for OR, comma separated
     * @param <type> $value One or several for OR, comma separated
     * @return <type>
     */
    public function addAttributeToFilter($attributeName, $operator, $value) {
        if (is_null($this->_productCollection))
            $this->_getProductCollection();
        $operators = explode(',', $operator);
        $values = explode(',', $value);
        if ($operator == 'IN') {
            $value = $values;
        } else if (count($operators) != count($values)) {
            return;
        }
        if (count($operators) == 1) {
            $this->_productCollection->addAttributeToFilter($attributeName, array($operator => $value));
        } else {
            $condition = array('or' => array());
            for ($i = 0; $i < count($operators); $i++) {
                $condition['or'][$i + 1] = array($operators[$i] => $values[$i]);
            }
            $this->_productCollection->addAttributeToFilter($attributeName, $condition);
        }
    }

    public function setLimit($limit, $offset = false) {
        if ($limit) {
            $this->_limit = $limit;
            $this->_offset = $offset;
            if (is_null($this->_productCollection))
                $this->_getProductCollection();
            if ($this->_productCollection) {
                if ($offset)
                    $this->_productCollection->getSelect()->limit($limit, $offset);
                else
                    $this->_productCollection->setPageSize($limit);//$this->_productCollection->getSelect()->limit($limit);
                $this->_toolbar = false;
                $this->reset();
                //Mage::log($this->_productCollection->getSelect().'', null, 'debuglist.log');
            }
        }
        return $this;
    }

    public function setOrder($field) {
        if ($field) {
            $this->_order = $field;
            if (is_null($this->_productCollection))
                $this->_getProductCollection();
            if ($this->_productCollection) {
                $this->_productCollection->setOrder($field);
                $this->reset();
            }
        }
    }
    
    public function setRandomOrder() {
        if (is_null($this->_productCollection)) {
            $this->_getProductCollection();
        }
        if($this->_productCollection) {
            $this->_productCollection->getSelect()->order(new Zend_Db_Expr('RAND()'));
            $this->reset();
        }
    }

    public function productsInAtLeastXCategories($min) {
        $min = intval($min);
        if ($min <= 0)
            return;
        if (is_null($this->_productCollection))
            $this->_getProductCollection();
        if ($this->_productCollection) {
            $this->_productCollection->getSelect()->joinLeft(
                    array('cpi' => $this->_productCollection->getTable('catalog/category_product_index')), 'e.entity_id = cpi.product_id and cpi.store_id = ' . Mage::helper('wcooall')->getCurrentStoreId() . ' and cpi.category_id != ' . Mage::helper('wcooall/category')->getRootCategoryId(), array('product_id', 'count_cat' => 'count(cpi.category_id)')
            );
            $this->_productCollection->getSelect()->joinRight(
                    array('cf' => $this->_productCollection->getTable('catalog/category_flat') . '_store_' . Mage::helper('wcooall')->getCurrentStoreId()), 'cf.entity_id = cpi.category_id and cf.is_active = 1', array('is_cat_active' => 'is_active')
            );

            $this->_productCollection->getSelect()->having('count(cpi.category_id) >= ?', $min);
            $this->_productCollection->getSelect()->group('cpi.product_id');
            //die ($this->_productCollection->getSelect());
            $this->reset();
        }
    }

    /**
     * Allow setMode('grid' or 'list') in layouts
     */
    public function getMode() {
        if ($this->getData('mode'))
            return $this->getData('mode');
        return $this->getToolbarBlock()->getCurrentMode();
    }

    public function reset() {
        if ($this->_productCollection && $this->_productCollection->isLoaded()) {
            $this->_productCollection->clear();
        }
    }

    protected function _beforeToHtml() {
        if ($this->_toolbar) {
            return parent::_beforeToHtml();
        }
        $toolbar = $this->getToolbarBlock();
        $this->_getProductCollection();
        if ($this->_productCollection) {

            if ($orders = $this->getAvailableOrders()) {
                $toolbar->setAvailableOrders($orders);
            }
            if ($sort = $this->getSortBy()) {
                $toolbar->setDefaultOrder($sort);
            }
            if ($dir = $this->getDefaultDirection()) {
                $toolbar->setDefaultDirection($dir);
            }

            if ($toolbar->getCurrentOrder()) {
                $this->_productCollection->setOrder($toolbar->getCurrentOrder(), $toolbar->getCurrentDirection());
            }
        }
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        return $this;
    }

    public function setCategoryId($categoryId) {
        if ($categoryId && is_numeric($categoryId) && $categoryId > 0) {
            $this->setData('category_id', $categoryId);
            $this->getLayer()->setData('category_id', $categoryId);
            $this->getLayer()->setData('current_category', Mage::getModel('catalog/category')->load($categoryId));
            $this->reset();
        }
    }
    
    
    
    public function prepareSortableFieldsByCategory($category) {
        if(!$category) {
            return $this;
        }
        return parent::prepareSortableFieldsByCategory($category);
    }

   
    
    
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();

            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

        }
        return $this->_productCollection;
    }
    
    public function getCategory() {
        if(!$this->getCategoryId()) {
            return false;
        }
        if(is_null($this->_category)) {
            $this->_category = Mage::getModel('catalog/category')->load($this->getCategoryId());
        }
        return $this->_category;
    }
    
    public function addIsInStockFilter() {
        if (is_null($this->_productCollection))
            $this->_getProductCollection();
        Mage::getSingleton('cataloginventory/stock')
            ->addInStockFilterToCollection($this->_productCollection);
        return $this;
    }
    
    
    
}