<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_System_Config_Button extends Mage_Adminhtml_Block_System_Config_Form_Field {
 
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->getButtonHtml($element);
    }

    public function getButtonUrl()
    {
        return '';
    }

    public function getButtonLabel() {
        return $this->helper('adminhtml')->__('Button');
    }
    
    public function getIsAjaxButton()
    {
        return true;
    }

    /**
     * Generate synchronize button html
     *
     * @return string
     */
    public function getButtonHtml($element)
    {
        $buttonId = $element->getId();
        $buttonOnclick = "javascript:setLocation('".$this->getButtonUrl()."'); return false;";
        if($this->getIsAjaxButton()) {
            $buttonOnclick = "javascript:new Ajax.Request('".$this->getButtonUrl()."'); return false;";
        }
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'id'        => $buttonId,
                'label'     => $this->getButtonLabel(),
                'onclick'   => $buttonOnclick
            ));

        return $button->toHtml();
    }

    protected function _toHtml() {
        return '';
    }

}
