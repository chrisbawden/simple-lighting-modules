<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Cron_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	

    public function __construct()
    {
        parent::__construct();
        $this->setId('wcooCronGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('identifier');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getSingleton('wcooall/cron_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

         $this->addColumn('identifier', array(
            'header'    => $this->__('Cron'),
            'index'     => 'identifier',
            'sortable'  => true,
        ));

        $this->addColumn('model', array(
            'header'    => $this->__('Model'),
            'index'     => 'model',
            'sortable'  => true,
        ));

        $this->addColumn('cron_expr', array(
            'header'    => $this->__('Cron Expression'),
            'index'     => 'cron_expr',
            'sortable'  => false,
            'filter'    => false
        ));

        $this->addColumn('action', array(
            'header'    =>  Mage::helper('wcooall')->__('Action'),
            'width'     => '100px',
            'type'      => 'action',
            'getter'    => 'getIdentifier',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('wcooall')->__('Execute'),
                    'url'       => array('base'=>'*/*/exec'),
                    'field'     => 'identifier'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
