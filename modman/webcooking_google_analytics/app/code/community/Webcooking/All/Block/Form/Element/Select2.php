<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Form_Element_Select2 extends Varien_Data_Form_Element_Select
{
    
    protected $_isMultiple = '0';
    
 
    
      public function getElementHtml()
    {
        $html = '
       <p>
            <input type="hidden"
                id = "' . $this->getHtmlId() . '"
                name = "' . $this->getName() . '"
                value = "' . $this->getEscapedValue() .  '"
                data-placeholder="' . Mage::helper('wcooall')->__('-- Please select --') . '" style="width:98%;"/>
        </p>';
        $defaultOptionText = $this->getDefaultOptionText();
        $defaultOptionText = trim(str_replace("'", "\'", $defaultOptionText));
        $value = trim($this->getValue());
        $searchText = Mage::helper('wcooall')->__('Search');
        $js = <<<EOF
<script type="text/javascript">
jQuery.noConflict()(document).ready(function() {
        jQuery.noConflict()('#{$this->getHtmlId()}').select2({
            minimumInputLength: 3,
            multiple: {$this->_isMultiple},
            placeholder: '{$searchText}',
            ajax: {
                url: '{$this->getLink()}',
                dataType: 'json',
                data: function(term, page) {
                    return {$this->getJsonData()};
                },
                results: function (data, page) {
                    return { results: data };
                }
            },
            initSelection : function (element, callback) {
                var data = {id: '{$value}', text: '{$defaultOptionText}'};
                callback(data);
            }
        });
    });
</script>
EOF;
                
        
        $html .= $this->getAfterElementHtml();
        $html .= $js;
        return $html;
    }
    
    public function getLink() {
        if(!$this->hasData('link')) {
            $this->setData('link', Mage::getModel('adminhtml/url')->getUrl('adminhtml/webcooking_select2'));
        }
        return $this->getData('link');
    }
    
    public function getDefaultOptionText() {
        if(!$this->hasData('default_option_text')) {
            if(Mage::registry('current_product')) {
                $this->setData('default_option_text', Mage::registry('current_product')->getAttributeText($this->getId())?Mage::registry('current_product')->getAttributeText($this->getId()):Mage::registry('current_product')->getData($this->getId()));
            }
            if(Mage::registry('current_category')) {
                $this->setData('default_option_text', Mage::registry('current_category')->getAttributeText($this->getId())?Mage::registry('current_category')->getAttributeText($this->getId()):Mage::registry('current_category')->getData($this->getId()));
            }
        }
        return $this->getData('default_option_text');
    }
    
    public function getJsonData() {
        if(!$this->hasData('json_data')) {
            $entityType = Mage::registry('current_product')?'catalog_product':'';
            if(!$entityType && Mage::registry('current_category')) {
                $entityType = 'catalog_category';
            }
            $this->setData('json_data', "{
                        search: term,
                        attribute_code: '{$this->getId()}',
                        entity_type: '{$entityType}'
                    }");
        }
        return $this->getData('json_data');
        
    }
}
