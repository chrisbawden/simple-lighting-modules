<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Form_Element_Multiselect2 extends Webcooking_All_Block_Form_Element_Select2
{
     protected $_isMultiple = '1';
}
