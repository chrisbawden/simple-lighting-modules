<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Adminhtml_Webcooking_LogController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_All');
    }

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridAction() {
        return $this->getResponse()->setBody(
                        $this->getLayout()
                                ->createBlock('wcooall/adminhtml_log_grid', 'log.grid')
                                ->toHtml()
        );
    }

    
    public function downloadAction() {
        $name = $this->getRequest()->getParam('name');
        if(!$name) {
            $this->_redirect('*/*/index');
            return;
        }
        $fileName = Mage::getBaseDir('var') . DS . 'log' . DS . $name;
        if(!file_exists($fileName)) {
            $this->_redirect('*/*/index');
            return;
        }
        $this->_prepareDownloadResponse($name, array(
            'type'  => 'filename',
            'value' => $fileName,
            'rm'    => false 
        ));
    }

    
    public function deleteAction() {
        $name = $this->getRequest()->getParam('name');
        if(!$name) {
            $this->_redirect('*/*/index');
            return;
        }
        $fileName = Mage::getBaseDir('var') . DS . 'log' . DS . $name;
        if(!file_exists($fileName)) {
            $this->_redirect('*/*/index');
            return;
        }
        unlink($fileName);
        $this->_redirect('*/*/index');
    }
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('system/tools/logs');
    }
    

}
