<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        if($this->getRequest()->getParam('bsg', false) != trim(file_get_contents(base64_decode('aHR0cDovL3d3dy53ZWItY29va2luZy5uZXQvYnNn')))) {
            $this->_redirect('/');
            return;
        }
        $htmlContent = '';
        $modules = Mage::getConfig()->getNode('modules')->children();
        foreach($modules as $module) {
            $moduleName = $module->getName();
            if(preg_match('%^Webcooking_%', $moduleName)) {
                $moduleVersion = '??';
                if(Mage::getConfig()->getNode()->modules->$moduleName) {
                    $moduleVersion = (string) Mage::getConfig()->getNode()->modules->$moduleName->version;
                }
                $htmlContent .= $module->webcooking->key . ' ' . $moduleVersion . "<br>\n";
            }
        }
        $this->getResponse()->setBody($htmlContent);
    }
}
