<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Product extends Mage_Core_Helper_Abstract {

    public function _getProduct($product) {
        if (is_object($product)) {
            return $product;
        }
        return Mage::getModel('catalog/product')->load($product);
    }

    public function getMinSaleQty($productId) {
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
        if ($stockItem->getMinQty() > 1) {
            return $stockItem;
        }
        if ($stockItem->getQtyIncrements() > 1) {
            return $stockItem->getQtyIncrements();
        }
        return 1;
    }

    public function isBundleProductWithoutConfiguration($product, $alwaysReturnArray = false) {
        $product = $this->_getProduct($product);
        if ($product->getTypeId() != 'bundle') {
            return $alwaysReturnArray?array():false;
        }
        
        $bundleProduct = clone $product;
        $additional = array();
        $isBundleProductWithoutConfiguration = true;
       
        $typeInstance = $bundleProduct->getTypeInstance(true);
        $typeInstance->setStoreFilter($bundleProduct->getStoreId(), $bundleProduct);
        $optionCollection = $typeInstance->getOptionsCollection($bundleProduct);

        $selectionCollection = $typeInstance->getSelectionsCollection(
                $typeInstance->getOptionsIds($bundleProduct), $bundleProduct
        );
        $options = $optionCollection->appendSelections($selectionCollection, false);
        $bundleOption = array();
        foreach ($options as $option) {
            $selections = $option->getSelections();
            if($selections == 1) {
                $selection = current($selections);
                $bundleOption[$option->getOptionId()] = $selection->getSelectionId();
            } else {
                foreach ($selections as $selection) {
                    if ($option->getRequired()) {
                        if ($selection->getIsDefault()) {
                            $bundleOption[$option->getOptionId()] = $selection->getSelectionId();
                        } else {
                            $isBundleProductWithoutConfiguration = false;
                            continue;
                        }
                    }
                }
            }
        }

        if (!$isBundleProductWithoutConfiguration) {
            return $alwaysReturnArray?array():false;
        }
        if(!empty($bundleOption)) {
            $additional['bundle_option'] = $bundleOption;
        }
        return $additional;
    }

}
