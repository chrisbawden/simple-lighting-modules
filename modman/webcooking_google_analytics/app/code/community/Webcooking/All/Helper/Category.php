<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Category extends Webcooking_All_Helper_Data {

    protected $_categoryIdsByName = array();
    protected $_categoryIdByName = array();
    protected $_categoryNameById = array();
    protected $_categoriesById = array();
    protected $_rootCategoryId = array();

    
    
    public function getRootCategoryId($storeId = false, $cached = true) {
        if(!$storeId) {
            $storeId = $this->getCurrentStoreId($cached);
        }
        if(!$cached) {
            return Mage::app()->getStore($storeId)->getRootCategoryId();
        }
        if(!isset($this->_rootCategoryId[$storeId])) {
            $this->_rootCategoryId[$storeId] = Mage::app()->getStore($storeId)->getRootCategoryId();
        }
        return $this->_rootCategoryId[$storeId];
    }
    
    protected function _loadAllCategories() {
        if (empty($this->_categoriesById)) {
            $categoryCollection = Mage::getModel('catalog/category')->getCollection();
            $categoryCollection->addAttributeToSelect('name');
            $categoryCollection->addAttributeToSelect('include_in_menu');
            $categoryCollection->addOrder('path', 'asc');
            foreach ($categoryCollection as $category) {
                $this->_categoriesById[$category->getId()] = $category;
                $name = array();
                $path = explode('/', $category->getPath());
                foreach ($path as $catId) {
                    if (isset($this->_categoriesById[$catId]) && $catId != 1 && $catId != $this->getRootCategoryId()) {
                        $name[] = $this->_categoriesById[$catId]->getName();
                    }
                }
                $name = implode('%', $name);
                $this->_categoryIdsByName[$name] = $path;
                $this->_categoryIdByName[$name] = $category->getId();
            }
            $this->_categoryNameById = array_flip($this->_categoryIdByName);
        }
    }
    
    
    public function getCategoryById($categoryId) {
        $this->_loadAllCategories();
        return $this->_categoriesById[$categoryId];
    }
    
    public function getCategoryNameById($categoryId, $separator = '%', $limit=false, $offset=0) {
        $this->_loadAllCategories();
        if (!array_key_exists($categoryId, $this->_categoryNameById)) {
            return false;
        }
        $categoryName = $this->_categoryNameById[$categoryId];
        if($limit) {
            $categoryName = explode('%', $categoryName);
            $categoryName = array_slice($categoryName, $offset, $limit, true);
            $categoryName = implode('%', $categoryName);
        }
        if($separator == '%') {
            return $categoryName;
        }
        if(!$separator) {
            return explode('%', $categoryName);
        }
        return str_replace('%', $separator, $categoryName);
    }


    public function getCategoryIdsByName($name, $importIfNotExists = false, $data = array()) {
        $this->_loadAllCategories();
        if (!array_key_exists($name, $this->_categoryIdsByName)) {
            if ($importIfNotExists) {
                $this->importCategory($name, $data);
                return $this->getCategoryIdsByName($name, false);
            } else {
                return false;
            }
        }
        return $this->_categoryIdsByName[$name];
    }
    
    public function getCategoryIdsById($categoryId) {
        $this->_loadAllCategories();
        $categoryName = $this->getCategoryNameById($categoryId);
        return $this->getCategoryIdsByName($categoryName);
    }
    

    public function getCategoryIdByName($name) {
        $this->_loadAllCategories();
        if (!array_key_exists($name, $this->_categoryIdByName)) {
            return false;
        }
        return $this->_categoryIdByName[$name];
    }
    
    public function getCategoriesAsOptionsArray($formated = false) {
         $this->_loadAllCategories();
         $categoriesOptions = array();
         foreach($this->_categoriesById as $categoryId =>$category) {
             if($categoryId > 1) {
                $categoriesOptions[$categoryId] = $this->getCategoryNameById($categoryId, ' > ');
             }
         }
         if($formated) {
             $categoriesFormatedOptions = array();
             foreach($categoriesOptions as $key=>$val) {
                 $categoriesFormatedOptions[] = array('value'=>$key, 'label'=>$val);
             }
             return $categoriesFormatedOptions;
         }
         return $categoriesOptions;
    }
    
   

    public function importCategory($name, $data) {
        $this->_loadAllCategories();
        $categoryApi = Mage::getModel('catalog/category_api');
        $nameParts = explode('%', $name);
        $curName = '';
        $parentId = $this->getRootCategoryId();
        for ($i = 0; $i < count($nameParts); $i++) {
            if ($i != 0)
                $curName .= '%';
            $curName .= $nameParts[$i];
            $result = $this->getCategoryIdsByName($curName, false);
            if (!$result) {
                $categoryData = array(
                    'name' => $nameParts[$i],
                    'is_active' => 1,
                    'include_in_menu' => 1,
                    'is_anchor' => 1,
                    'available_sort_by' => false,
                    'default_sort_by' => false,
                    'path' => $this->_categoriesById[$parentId]->getPath()
                );
                foreach ($data as $k => $v)
                    $categoryData[$k] = $v;
                try {
                    $categoryId = $categoryApi->create($parentId, $categoryData);
                    $this->_categoriesById[$categoryId] = Mage::getModel('catalog/category')->load($categoryId);
                    $this->_categoryIdsByName[$curName] = explode('/', $this->_categoriesById[$categoryId]->getPath());
                    $parentId = $categoryId;
                } catch (Mage_Api_Exception $mae) {
                    throw $mae;
                }
            } else {
                $parentId = array_pop($result);
            }
        }
    }

}
