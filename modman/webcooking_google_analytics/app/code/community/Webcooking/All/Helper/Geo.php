<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Geo extends Mage_Core_Helper_Abstract
{

    public function DMSStringtoDEC($string) {
        if(preg_match('%([0-9]+)°([0-9]+)\'([0-9]+)"([A-Z]+)%i', $string, $pregResults)) {
            return $this->DMStoDEC($pregResults[1], $pregResults[2], $pregResults[3]);
        }
        return $string;
    }
    
    /* Converts DMS ( Degrees / minutes / seconds ) 
     * to decimal format longitude / latitude
     */
    public function DMStoDEC($deg,$min,$sec) {

        return $deg + ((($min * 60) + ($sec)) / 3600);
    }

    /* Converts decimal longitude / latitude to DMS
     * ( Degrees / minutes / seconds ) 
     * This is the piece of code which may appear to 
     * be inefficient, but to avoid issues with floating
     * point math we extract the integer part and the float
     * part by using a string function.
     */
    public function DECtoDMS($dec) {
        $vars = explode(".", $dec);
        $deg = $vars[0];
        $tempma = "0." . $vars[1];

        $tempma = $tempma * 3600;
        $min = floor($tempma / 60);
        $sec = $tempma - ($min * 60);

        return array("deg" => $deg, "min" => $min, "sec" => $sec);
    }

}
