<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Cron extends Mage_Core_Helper_Abstract {

    
    public function addTask($name, $taskMethod, $params=array(), $priority=1, $isUnique = false, $scheduleDate=false) {
        
        if(!is_array($params)) {
            throw new Exception('Cron task params should be an array.');
        }
        
        $serializedParams = Mage::getSingleton('wcooall/task')->serializeParams($params);
        
        
        if($isUnique) {
            $taskCollection = Mage::getModel('wcooall/task')->getCollection();
            $taskCollection->addFieldToFilter('name', $name);
            $taskCollection->addFieldToFilter('method', $taskMethod);
            $taskCollection->addFieldToFilter('params', $serializedParams);
            $taskCollection->addFieldToFilter('status', Mage_Cron_Model_Schedule::STATUS_PENDING);
            if($taskCollection->getSize() > 0) {
                return false;
            }
        }
        
        $task = Mage::getModel('wcooall/task');
        $task->setName($name);
        $task->setMethod($taskMethod);
        $task->setParams($serializedParams);
        $task->setStatus(Mage_Cron_Model_Schedule::STATUS_PENDING);
        $task->setPriority($priority);
        $task->setScheduledAt($scheduleDate?$scheduleDate:Mage::getSingleton('core/date')->date('Y-m-d H:i:s'));
        $task->save();
        
        return true;
        
    }

}
