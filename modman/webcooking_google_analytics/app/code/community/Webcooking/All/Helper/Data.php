<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Data extends Mage_Core_Helper_Abstract {

    
    protected $_defaultStore = null;
    protected $_currentStore = null;
    protected $_isCurrentlySecure = null;
    
    public function getDefaultStoreId($cached = true) {
        return  $this->getDefaultStore($cached)->getId();
    }
    
    public function getDefaultStore($cached = true) {
        if(!$cached) {
            return Mage::app()->getDefaultStoreView();
        }
        if(is_null($this->_defaultStore)) {
            $this->_defaultStore = Mage::app()->getDefaultStoreView();
        }
        return $this->_defaultStore;
    }
    
    public function isCurrentlySecure($storeId = null, $cached = true) {
        if(!$cached) {
            return Mage::app()->getStore($storeId)->isCurrentlySecure();
        }
        if(is_null($this->_isCurrentlySecure)) {
            $this->_isCurrentlySecure = (int)Mage::app()->getStore($storeId)->isCurrentlySecure();
        }
        return $this->_isCurrentlySecure;
    }
    
    public function getCurrentStoreId($cached = true) {
        return $this->getCurrentStore($cached)->getId();
    }
    
    public function getCurrentStore($cached = true) {
        if(!$cached) {
            return Mage::app()->getStore();
        }
        if(is_null($this->_currentStore)) {
            $this->_currentStore = Mage::app()->getStore();
        }
        return $this->_currentStore;
    }
    
    
    
    public function isModuleInstalled($module) {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;

        return isset($modulesArray[$module]);
    }
    
    public function isModuleEnable($module) {
        return $this->isModuleEnabled($module);
    }

    public function applyReplaceAccent($data) {
        $data = @iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $data = htmlentities($data, ENT_COMPAT, 'UTF-8');
        $data = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $data);
        $data = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $data); // pour les ligatures e.g. '&oelig;'
        $data = preg_replace('#\&[^;]+\;#', '', $data); // supprime les autres caractères
        return $data;
    }
    
    public function sanitizeUrlKey($urlKey) {
        $urlKey = $this->applyReplaceAccent($urlKey);
        $urlKey = preg_replace('%[^a-zA-Z0-9-]%i', '-', strtolower($urlKey));
        $urlKey = preg_replace('%--+%i', '-', $urlKey);
        $urlKey = preg_replace('%^-|-$%i', '', $urlKey);
        return $urlKey;
    }
    
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product : Product to check
     * @param integer $decimals
     * @param boolean $withSymbol
     * @param boolean $allowEmptyEndDate
     * @return boolean|string promo percentage or false if no promo
     */
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product)
            return false;

        if (!$product->getSpecialPrice())
            return false;

        if ($product->getTypeId() == 'bundle') {
            $percentage = (100 - $product->getSpecialPrice());
        } else {
            $percentage = (100 - round((100 * $product->getSpecialPrice()) / $product->getPrice(), $decimals));

            if ($product->getSpecialPrice() >= $product->getPrice())
                return false;
        }
        $now = time();
        $aDay = 24 * 60 * 60;
        if (!$allowEmptyEndDate && !trim($product->getSpecialToDate()))
            return false;
        if (trim($product->getSpecialToDate()) && $now >= strtotime($product->getSpecialToDate()) + $aDay)
            return false;
        if (trim($product->getSpecialFromDate()) && $now < strtotime($product->getSpecialFromDate()))
            return false;
        if ($product->getTypeId() != 'bundle' && $product->getPrice() <= 0)
            return true;
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
    }

    public function encrypt($value) {
        return openssl_encrypt($value, 'aes256', (string)Mage::getConfig()->getNode('global/crypt/key'));
    }
    
    public function decrypt($value) {
        return openssl_decrypt($value, 'aes256', (string)Mage::getConfig()->getNode('global/crypt/key'));
    }
    
    public function sendAlert($subject, $content, $to = false) {
        if(!$to) {
            $to = Mage::getStoreConfig('wcooall/alerts/to');
        }
        if(!$to) {
            return;
        }
        $tpl = Mage::getModel('core/email_template');
        $tpl->setDesignConfig(array('area'=>'adminhtml'))
            ->sendTransactional(
                Mage::getStoreConfig('wcooall/alerts/template'),
                Mage::getStoreConfig('wcooall/alerts/from'),
                $to,
                $to,
                array(
                    'subject'  => $subject,
                    'content'  => $content,
                )
        );
        
    }

}