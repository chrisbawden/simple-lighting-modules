<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
/**
 * @deprecated
 */
class Webcooking_All_Model_Form_Element_Select2 extends Webcooking_All_Block_Form_Element_Select2
{
    
}
