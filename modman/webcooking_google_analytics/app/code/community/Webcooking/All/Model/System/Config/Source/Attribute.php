<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Model_System_Config_Source_Attribute
{
    
    protected function _isAttributeAvailable($attribute) {
        return true;
    }
    
    public function toOptionArray()
    {
       $model = Mage::getResourceModel('catalog/product');
        $typeId = $model->getTypeId();

        $attributesCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                ->setEntityTypeFilter($typeId)
                ->setOrder('attribute_code', 'asc')
                ->load();
        $attributes = array();
        $attributes[] = array('value' => '', 'label' => '');
        foreach ($attributesCollection as $attribute) {
            if($this->_isAttributeAvailable($attribute)) {
                $code = $attribute->getAttributeCode();
                $attributes[] = array('value' => $code, 'label' => $code);
            }
        }

        return $attributes;
    }
}
