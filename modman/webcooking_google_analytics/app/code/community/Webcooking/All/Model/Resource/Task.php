<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */

class Webcooking_All_Model_Resource_Task extends Mage_Core_Model_Mysql4_Abstract
{
  public function _construct()
    {    

        $this->_init('wcooall/task', 'task_id');
    }
}