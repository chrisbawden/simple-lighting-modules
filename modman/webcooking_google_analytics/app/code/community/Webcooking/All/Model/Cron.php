<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

class Webcooking_All_Model_Cron  {

   public function executeTasks() {
       $taskCollection = Mage::getModel('wcooall/task')->getCollection();
       $taskCollection->addFieldToFilter('status', Mage_Cron_Model_Schedule::STATUS_PENDING);
       $taskCollection->addFieldToFilter('scheduled_at', array('lteq'=>Mage::getSingleton('core/date')->date('Y-m-d H:i:s')));
       $taskCollection->getSelect()->order('priority ASC')->order('scheduled_at ASC');
       foreach($taskCollection as $task) {
           $task->execute();
       }
   }
   
   
}
