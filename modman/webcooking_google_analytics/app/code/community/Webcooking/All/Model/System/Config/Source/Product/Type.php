<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Model_System_Config_Source_Product_Type extends Varien_Object {

    public function toOptionArray()
    {
        return array(
            array('value'=>'simple', 'label'=>'simple'),
            array('value'=>'virtual', 'label'=>'virtual'),
            array('value'=>'configurable', 'label'=>'configurable'),
            array('value'=>'bundle', 'label'=>'bundle'),
            array('value'=>'grouped', 'label'=>'grouped'),
            array('value'=>'downloadable', 'label'=>'downloadable'),
        );
    }
}
