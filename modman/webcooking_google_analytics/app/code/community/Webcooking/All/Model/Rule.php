<?php
/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
if(class_exists('Mage_Rule_Model_Condition_Product_Abstract', false)) {
    require_once 'Webcooking/All/Model/RuleCompatibility/Mage_Rule_Model_Abstract.php';
} else {
    require_once 'Webcooking/All/Model/RuleCompatibility/Webcooking_All_Model_Rule_Abstract.php';
}


class Webcooking_All_Model_Rule extends Webcooking_All_Model_Rule_Compatibility {

    public function getConditionsInstance()
    {
        return Mage::getModel('wcooall/rule_condition_combine');
    }
    
    
    public function getActionsInstance()
    {
        return Mage::getModel('wcooall/rule_condition_combine');
    }
    
    public function applyConditionToProductCollection($productCollection, $defaultCondition="1 = 0") {
        
        $conditions = $this->getConditions();
        $conditions->applyConditionToProductCollection($productCollection, $defaultCondition);
        return $productCollection;
    }
    
}
