<?php

/**
 * Copyright (c) 2011-2018 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleApi_Helper_Data extends Webcooking_All_Helper_Data {
        
    public function log($message, $module = 'googleapi') {
        Mage::log($message, null, 'googleapi_'.$module.'.log');
    }  
    
    public function logException($exception, $module = 'googleapi') {
        $this->log($exception->getMessage(), $module);
        $this->log($exception->getTraceAsString(), $module);
    }

}